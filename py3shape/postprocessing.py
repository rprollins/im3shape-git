import numpy as np
from .image import Image
from . import utils
from . import structs
import warnings

class ModelFitInformation(object):
    def __init__(self, result, options, pixel_scale=utils.DES_PIXEL_SCALE):
        self.best_fit_params = result.get_params()
        self.options = options
        self.rounded_params = self.rounded_parameters(self.best_fit_params, options, pixel_scale, high_res=False)
        self.rounded_params_highres = self.rounded_parameters(self.best_fit_params, options, pixel_scale, high_res=True)
        self.pixel_scale = pixel_scale

    @staticmethod
    def rounded_parameters(params, options, pixel_size, high_res):
        #These parameters are also scaled up so that the model they
        #make is at higher resolution
        upsampling = options.upsampling
        sz = (options.stamp_size+options.padding)*options.upsampling
        rp = structs.sersics_parameter_set()
        rp.e1 = 0.0
        rp.e2 = 0.0
        if high_res:
            rp.x0 = sz/2 + 0.5
            rp.y0 = sz/2 + 0.5
            rp.radius = params.radius*options.upsampling/pixel_size
        else:
            rp.x0 = options.stamp_size/2 + 0.5
            rp.y0 = options.stamp_size/2 + 0.5
            rp.radius = params.radius/pixel_size
        #This needs to be in here too because the WCS
        #also changes the amplitudes
        if hasattr(params, "bulge_A"):
            rp.bulge_A = params.bulge_A * pixel_size**2
            rp.disc_A = params.disc_A * pixel_size**2
        else:
            rp.bulge_A = 0.0
            rp.disc_A = 1.0
            warnings.warn("The rounded SNR parameter is incorrect now for non-disc models.")

        rp.bulge_index = params.bulge_index
        rp.disc_index = params.disc_index
        rp.radius_ratio = params.radius_ratio
        rp.delta_e_bulge = 0.0
        rp.delta_theta_bulge = 0.0
        return rp

    @staticmethod
    def deshear(image_array, g1, g2, pixel_scale):
        import galsim

        psf_model = galsim.InterpolatedImage(galsim.Image(image_array, dtype=np.float64), 
            scale=pixel_scale)
        #shear the psf image with the inverse of the previously calculated PSF params
        round_psf_model = psf_model.shear(g1=-g1, g2=-g2)

        #Make a np image and then an im3shape image from the new rounded PSF
        nx, ny = image_array.shape
        round_psf_image = galsim.ImageD(nx, ny)
        round_psf_model.draw(round_psf_image, scale=pixel_scale)
        round_psf = Image(round_psf_image.array)
        return round_psf

    def measure_psf(self, psf, results):
        #Get the PSF FWHM.
        #This PSF will already be at high resolution so
        #nothing needs to be changed here
        results['psf_fwhm'] = utils.get_FWHM(psf, fwxm=0.5, 
            upsampling=self.options.upsampling, 
            radialProfile=True)

        #Get the PSF ellipticity 
        moments = psf.weighted_moments(weight_radius=10.)
        results['psf_e1'] = moments.e1
        results['psf_e2'] = moments.e2


        #Get the PSF HSM parameters
        #PSF is high-res so we have to tell hs to
        #reduce the sigma value.
        hsm = get_hsm_params(psf, upsampling=self.options.upsampling)
        results['hsm_psf_sigma'] = hsm.moments_sigma
        results['hsm_psf_e1'] = hsm.observed_shape.g1
        results['hsm_psf_e2'] = hsm.observed_shape.g2
        results['hsm_psf_rho4'] = hsm.moments_rho4
        results['hsm_psf_status'] = hsm.moments_status
        results['hsm_psf_niter'] = hsm.moments_n_iter
        
    def measure_round_snr(self, psf_array, image, original_model, weight, results):

        #Now we need a rounded PSF image too ...
        #Use galsim to make an interpolating psf image
        round_psf = self.deshear(psf_array, 
            results['hsm_psf_e1'], results['hsm_psf_e2'], 
            self.pixel_scale/self.options.upsampling)

        #Make a new convolved model image with the rounded psf and parameters.
        #This time we do not want a high-res model since it should be
        #the same as the weight and the image data
        round_model = Image.create_sersics_model_image_with_psf_image(
            self.rounded_params, round_psf, self.options)

        #Measure the SNR of the new convolved model image
        model = round_model.array()
        results['round_snr'] = np.sqrt((weight*model*model).sum())

        #We also do a version where we calculate a mean weight factor
        #to avoid a possible bias when rounding off elliptical galaxies
        #changes the amount of them covered by mask.
        #This is actually a weighted mean of the weight
        #The abs is just in case of negative models, which happen occasionally.
        mean_weight = (abs(original_model)*weight).sum() / abs(original_model.sum())
        results['round_snr_mw'] = np.sqrt((mean_weight*model*model).sum())


    def measure_fwhm(self, psf, results):
        stamp_large = (self.options['stamp_size']+self.options['padding'])*self.options['upsampling']
        #Generate a realization of the fitted model but with the
        #galaxy rounded to e1 = e2 = 0.
        #This needs to be at high resolution, so we need to temporarily
        #change the upsampling to 1, padding to 0, and stamp_size to the
        #full size:
        #use the context manager above that I just wrote
        with utils.temporary_values(self.options, stamp_size=stamp_large, upsampling=1, padding=0):
            high_res_model = Image.create_sersics_model_image_with_psf_image(
                self.rounded_params_highres, psf, self.options)

        #And the FWHM from Tomek's code.
        results['fwhm'] = utils.get_FWHM(high_res_model, upsampling=self.options.upsampling)
        results['rgpp_rp'] = results['fwhm']/results['psf_fwhm']

    def measure_flux_fractions(self, image, model, weight, uber, results):
        #Flux fractions
        results['unmasked_flux_frac'] = utils.get_unmasked_flux_fraction(model, weight)
        results['unmasked_img_flux_frac'] = utils.get_unmasked_flux_fraction(image, weight)
        if uber is None:
            results['un_uber_flux_frac_'] = 0.0
            results['un_uber_img_flux_frac'] = 0.0
        else:
            results['un_uber_flux_frac_'] = utils.get_unmasked_flux_fraction(model, uber)
            results['un_uber_img_flux_frac'] = utils.get_unmasked_flux_fraction(image, uber)

        results['mask_fraction'] = ((weight==0).sum() * 1.0) / weight.size

    def measure_edge(self, image, model, weight, results):
        #Edge stats
        results['edge_mu'], results['edge_sigma'] = utils.get_image_edge_stats(image, weight)
        results['model_edge_mu'], results['model_edge_sigma'] = utils.get_image_edge_stats(model, weight)


    def measure(self, psf_array, image, model, weight, uber=None):
        import galsim
        results = {}
        psf = Image(psf_array)

        self.measure_psf(psf, results)
        self.measure_round_snr(psf_array, image, model, weight, results)
        self.measure_fwhm(psf, results)
        self.measure_flux_fractions(image, model, weight, uber, results)
        self.measure_edge(image, model, weight, results)
        return results


def get_hsm_params(obj, weight=None, upsampling=1.):
    """
    @brief
    Computes PSF parameters from potentially higher-resolved PSF image.
    In particular it computes e1, e2 and size for the provided object
    @obj i3_image image of the PSF with correct resolution (should have been used earlier for fitting)
    @return hsm_params - hsm struct 
    """
    import galsim
    
    # create a galsim object
    def make_galsim_img(x):
        arr = np.ascontiguousarray(obj.array().astype(np.float64))
        return galsim.Image(arr, dtype=np.float64)
    Image_obj = make_galsim_img(obj)
    if weight != None:
        weight = make_galsim_img(weight)
        
    try:
        result = galsim.hsm.FindAdaptiveMom(Image_obj, weight=weight, strict=False)
        result.moments_sigma = result.moments_sigma / upsampling
    except:
        result = galsim.hsm.ShapeData()            

    return result
