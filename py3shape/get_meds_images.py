import py3shape
import analyze_meds
import numpy as np
import argparse
import sys

def extract_params_from_row(row, options):
	ID=int(row[0])
	options.e1_start = row[3]
	options.e2_start = row[4]
	options.ra_as_start = row[9]
	options.dec_as_start = row[10]
	options.radius_start = row[11]
	options.bulge_A_start = row[12]
	options.disc_A_start = row[13]
	return ID

def main(args):
	# using code from analyze_meds, prepare for run by finding image data
    meds = analyze_meds.I3MEDS(args.file_input_meds)
    # we will use a real psf
    use_psfex = True
    #options from ini file except we force save_images
    #and force the use of the params directly from the file
    options=py3shape.Options(args.file_input_ini)
    options.save_images=True
    options.use_computed_start=False

    im3shape_cat = np.loadtxt(args.file_input_im3shape_results)

    start = args.start
    end = min(start+args.count, len(im3shape_cat))
    print "Running objects from %d to %d" % (start, end)
    for i in xrange(start, end):
    	#this also sets the options to use the starting position from 
    	object_id = extract_params_from_row(im3shape_cat[i], options)
	print "Running object in row %d with ID %d" % (i,object_id)
    	#we are going to run this but just ignore the result as we do not need it.
    	meds.im3shape_analyze(options, use_psfex, object_id, starting_params_only=True)

if __name__=="__main__":


    # Set up and parse the command line arguments using the nice Python argparse module
    description = "Run on a completed im3shape output file to re-run and save best-fit images."
    parser = argparse.ArgumentParser(description=description, add_help=True)

    parser.add_argument('file_input_meds', type=str, help='the input meds FITS file')
    parser.add_argument('file_input_ini', type=str, help='the name of the existing im3shape results file to get images from')
    parser.add_argument('file_input_im3shape_results', type=str, help='the name of the existing ini file to be used with im3shape')
    parser.add_argument('--count', '-n', type=int, action='store', default=100000000, help='max number of params to do')    
    parser.add_argument('--start', '-s', type=int, action='store', default=0, help='Index of starting param')    

    args = parser.parse_args()
    main(args)
    sys.exit(0)
