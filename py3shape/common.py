import os
import sys
import ctypes as ct
import numpy as np
import ctypes

if getattr(sys, 'frozen', False):
    libim3 = ct.CDLL('libim3.so', mode=ct.RTLD_GLOBAL)
else:
    libim3 = ct.CDLL(os.path.join(os.path.split(__file__)[0],'libim3.so'), mode=ct.RTLD_GLOBAL)



if ct.c_int.in_dll(libim3, "i3UseDouble"):
    py_i3_flt = np.float64
    py_i3_cpx = np.complex128
    c_i3_flt = ct.c_double
else:
    py_i3_flt = np.float32
    py_i3_cpx = np.complex64
    c_i3_flt = ct.c_float
   
c_i3_flt_p = ct.POINTER(c_i3_flt)
i3_list_of_models = ct.c_char_p.in_dll(libim3, "i3ListOfModels").value.strip().split(' ')
MAX_STRING_LEN=ct.c_int.in_dll(libim3, "i3_max_string_length_export").value
MAX_N_EXPOSURE = ct.c_int.in_dll(libim3, "i3_max_number_exposures").value
