from .image import Image
from .results import Results
from .options import Options
from .analyze import analyze, get_FWHM, compute_rgpp_rp
from . import structs
from . import lib
from . import utils
import time
import sys
import pdb

def test_memory():
    options = Options("./example/params.ini")
    options.padding=0
    options.upsampling=1
    options.stamp_size=100
    options.x0_min=45
    options.x0_max=55
    options.y0_min=45
    options.y0_max=55
    lib.i3_gsl_init_rng()
    psf = utils.small_round_psf_image(100)
    for i in xrange(1):
        img=Image(100, 100)
        img.add_sersic(10.0, 0.0, 0.0, 1.0, 50.0, 50.0, 1.0)
        img.add_white_noise(0.1)
        result, fit = analyze(img, psf, options)
        print result.get_params()
        print


def test_get_FWHM():
    """
    Test script for the computation of rgpp_rp
    """
    start = time.clock()
    
    # create simple toy example: Moffat PSF and simple one-component sersic galaxy image
    options = Options("./example/params.ini")
    options.verbosity = -1
    
    nx = ny = options.stamp_size
    beta = 3.
    fwhm = 2.85
    e1 = 0.0
    e2 = 0.0

    psf = Image.make_great10_psf(nx, ny, beta, fwhm, e1, e2, options)
    fwhm_psf = get_FWHM(psf, upsampling=options.upsampling)

    print '\n Test for FWHM computation \n'
    print 'PSF -------------------------------------------------'
    print 'True FWHM: \t\t\t\t\t %.2f' % fwhm
    print 'Estimated FWHM: \t\t\t\t %.2f\n' % fwhm_psf

    # pure exponential
    # -----
    # re=1.3
    # Rgp/Rp = 1.4147
    # Rgpp/Rp = 1.4447
    #  
    # pure devauc 
    # -----
    # re=3.8
    # Rgp/Rp =   1.4497
    # Rgpp/Rp = 1.4909

    # Set up parameter struct for pure DeVaucoleur
    sersics_params = structs.sersics_parameter_set()
    sersics_params.x0 = nx/2.
    sersics_params.y0 = ny/2.
    sersics_params.e1 = 0.
    sersics_params.e2 = 0.
    sersics_params.bulge_A = 1.
    sersics_params.disc_A = 0.
    sersics_params.bulge_index = 4
    sersics_params.disc_index = 1
    sersics_params.radius = 3.8
    sersics_params.radius_ratio = 1.
    sersics_params.delta_e_bulge = 0.
    sersics_params.delta_theta_bulge = 0.
    
    gal = Image.create_sersics_model_image_with_psf_image(sersics_params, psf, options)
    fwhm_gal = get_FWHM(gal)

    # fit pure bulge only
    options.sersics_disc_A_start = 0.
    options.sersics_disc_A_fixed = True
    result, best_fit = analyze(gal, psf, options)
    #rgpp_rp = compute_rgpp_rp(result, best_fit, psf, options)
    rgpp_rp, est_fwhm_gal, est_fwhm_psf = compute_rgpp_rp(result, psf, options)

    print 'Pure DeVaucoleur ------------------------------------'    
    print 'True FWHM: \t\t\t\t\t %.2f' % 1.4909
    print 'Estimated rgpp_rp of noise-free galaxy image: \t %.2f' % (fwhm_gal/fwhm_psf)
    print 'Estimated rgpp_rp of fitted model image: \t %.2f\n' % rgpp_rp

    # Set up parameter struct for pure Exponential
    sersics_params.bulge_A = 0.
    sersics_params.disc_A = 1.
    sersics_params.radius = 1.3

    # fit pure disc only
    gal = Image.create_sersics_model_image_with_psf_image(sersics_params, psf, options)
    fwhm_gal = get_FWHM(gal)

    options.sersics_disc_A_start = 1.
    options.sersics_disc_A_fixed = False
    options.sersics_bulge_A_start = 0.
    options.sersics_bulge_A_fixed = True
    result, best_fit = analyze(gal, psf, options)
    rgpp_rp, est_fwhm_gal, est_fwhm_psf = compute_rgpp_rp(result, psf, options)

    print 'Pure Exponential ------------------------------------'    
    print 'True rgpp_rp: \t\t\t\t\t %.2f' % 1.4447
    print 'Estimated rgpp_rp of noise-free galaxy image: \t %.2f' % (fwhm_gal/fwhm_psf)
    print 'Estimated rgpp_rp of fitted model image: \t %.2f\n' % rgpp_rp

    stop = time.clock()
    print 'Done in %.2f sec.' % (stop - start)
    
if __name__=="__main__":
    test_get_FWHM()
    
    # test_memory()
