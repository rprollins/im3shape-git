I3_COMPLETE = 0
I3_INCOMPLETE = -1
I3_UNKNOWN_ERROR = -2
I3_UNKNOWN_GALAXY_ERROR = 1

class I3Error(Exception):
    i3_code = I3_UNKNOWN_ERROR

#These negative ones are errors that should warrant further attention
#they are not just individual galaxy errors and are likely to recurr or 
#indicate that something is wrong with the input parameters

class I3NoteworthyError(I3Error):
    i3_code = I3_UNKNOWN_ERROR

class I3StructuralError(I3NoteworthyError):
    i3_code = -3

class I3ParameterError(I3NoteworthyError):
    i3_code = -4

class I3FileExistsError(I3NoteworthyError):
    i3_code = -5


#These errors are expected to pop up for some individual 
#galaxies within a run

class I3GalaxyError(I3Error):
    i3_code = I3_UNKNOWN_GALAXY_ERROR


class I3StampTooLarge(I3GalaxyError):
    i3_code = 2

class I3TooManyExposures(I3GalaxyError):
    i3_code = 3

class I3NoExposures(I3GalaxyError):
    i3_code = 4

class I3NoWeight(I3GalaxyError):
    i3_code = 5

class I3MissingTiling(I3GalaxyError):
    i3_code = 6

class I3MissingPSFFile(I3GalaxyError):
    i3_code = 7

class I3NoPSFs(I3GalaxyError):
    i3_code = 8

class I3MissingPrecomputedResults(I3GalaxyError):
    i3_code = 9

class I3UbersegMissing(I3GalaxyError):
    i3_code = 10

class I3PostprocessModelFitError(I3GalaxyError):
    i3_code = 11
    
class I3RejectingAnyBlacklist(I3GalaxyError):
    i3_code = 12
