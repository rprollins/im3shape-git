import numpy as np
import os
from .options import default_options
from .utils import singleton_connection
from .errors import *


class Output(object):
	def flush(self):
		pass

class TextOutput(Output):
	def __init__(self, 
			main_file, epoch_file,
			main_header=[], epoch_header=[],
			):
		self.headers = {}
		self.main_cat = self.open_cat(main_file,  main_header)
		self.epoch_cat = self.open_cat(epoch_file,  epoch_header)
		self.started=False
		self.buffered_errors = []

	def open_cat(self, filename, header):
		if os.path.exists(filename) and os.stat(filename).st_size> 0:
			f = open(filename, 'a')
			self.headers[f] = []
		else:
			f = open(filename, 'w')
			self.headers[f] = header
		return f

	def init_cat(self, f, columns):
		header = self.headers[f]
		f.write('#')
		for col in columns:
			f.write(col + '    ')
		f.write('\n')
		for line in header:
			f.write('# '+line+'\n')
		return f

	def flush(self):
		self.main_cat.flush()
		self.epoch_cat.flush()

	def try_write_buffered_errors(self):
		if not self.started:
			return
		for ID, code in self.buffered_errors:
			self.write_failure(ID, code)
		self.buffered_errors = []

	def write_failure(self, ID, code):
		if not self.started:
			self.buffered_errors.append((ID, code))
			return
		for name, vtype in zip(self.main_columns, self.main_types):
			if name=="identifier" or name=="id":
				self.main_cat.write('%s    '%ID)
			elif name=="status":
				self.main_cat.write('%s    '%code)
			elif vtype==np.float64 or vtype==np.float32 or vtype==float:
				self.main_cat.write('NaN    ')
			elif vtype==np.int64 or vtype==np.int32 or vtype==int or vtype==long:
				self.main_cat.write('-9999    ')
			elif vtype==str:
				self.main_cat.write('?????    ')
			else:
				raise ValueError("Unknown type for write_failure: {}".format(vtype))
		self.main_cat.write("\n")




	def write_row(self, main, epochs):
		if not self.started:
			self.main_columns = main.keys()
			self.main_types = [type(val) for val in main.values()]
			self.init_cat(self.main_cat, self.main_columns)
			if epochs:
				self.epoch_columns = epochs[0].keys()
				self.init_cat(self.epoch_cat, ["ID", "expnum"]+self.epoch_columns)
			self.started = True

		for col in self.main_columns:
			self.main_cat.write('%s    '%main[col])
		self.main_cat.write("\n")

		for i,epoch in enumerate(epochs):
			self.epoch_cat.write('%s    '%main['identifier'])
			self.epoch_cat.write('%s    '%i)
			for col in self.epoch_columns:
				self.epoch_cat.write('%s    '%epoch[col])
			self.epoch_cat.write("\n")


class HDF5Output(Output):
	def __init__(self, filename, main_size, epoch_size, main_row=0, epoch_row=0):
		import h5py

		self.started = False
		self.main_size = main_size
		self.epoch_size = epoch_size
		self.main_row = main_row
		self.epoch_row = epoch_row

		if os.path.exists(filename):
			raise I3FileExistsError("File {} already exists - please delete".format(filename))
		
		self.file = h5py.File(filename, 'w')
		self.main_cat = self.file.create_group("main")
		self.epoch_cat = self.file.create_group("epoch")

	def flush(self):
		self.file.flush()

	def init_output(self, data, group, size):
		for name, val in data.items():
			if type(val) in (int,long,np.int32,np.int64):
				dtype = np.int64
			elif type(val) in (float,np.float32,np.float64):
				dtype = np.float64
			elif type(val)==str:
				dtype = 's24'
			else:
				raise I3StructuralError("Unknown data type %s=%s  (%s)"%(col, val, type(val)))

			group.create_dataset(name, (size,), dtype=dtype) 

	def write_row(self, main, epochs):
		if not self.started:
			self.main_columns = main.keys()
			self.epoch_columns = epochs[0].keys()
			self.init_output(main, self.main_cat, self.main_size)
			self.init_output(epochs[0], self.epoch_cat, self.epoch_size)

		for name, val in main.items():
			self.main_cat[name][self.main_row] = val
		self.main_row += 1

		for epoch in epochs:
			for name, val in epoch.items():
				self.epoch_cat[name][self.epoch_row] = val
			self.epoch_row += 1



class DatabaseOutput(Output):
	def __init__(self, base_name,
			hostname='scidb1.nersc.gov', database='des_im3shape'
			):
		self.base_name = base_name
		self.main_name = base_name+"_main"
		self.epoch_name = base_name+"_epoch"
		self.connection = singleton_connection(hostname, database)

	def create_db_sql(self, name, columns):
		define = """
		CREATE OR REPLACE FUNCTION i3_create_{0} ()
		  RETURNS void AS
		$_$
		BEGIN

		IF NOT EXISTS (
		    SELECT *
		    FROM   pg_catalog.pg_tables 
		    WHERE  schemaname = 'public'
		    AND    tablename  = '{0}'
		    ) THEN
		   CREATE TABLE {0} ({1});
		END IF;

		END;
		$_$ LANGUAGE plpgsql;
		""".format(name.lower(), ','.join(columns))
		return define

	def create_db(self, name, columns):
		define = self.create_db_sql(name, columns)
		cursor = self.connection.cursor()
		cursor.execute(define)
		cursor.execute("SELECT i3_create_{0}();".format(name))
		self.connection.commit()
		cursor.close()


	def init_cat(self, table_name, outputs, main):
		#The documentation for postgresql says:
		#"Never, never, NEVER use Python string concatenation (+) or string parameters 
		#interpolation (%) to pass variables to a SQL query string. Not even at gunpoint"
		#
		#But that demand is stupid in our case - it's for the case where you have some 
		#kind of input from sources external to the running program, i.e. if you run a website 
		#or a DB for products or addresses instead of having a proper job like  running 
		#galaxy shape measurement codes on supercomputers.
		#
		#Hopefully, anyway.
		#
		column_sql = []
		if not main:
			column_sql.append("ID bigint")
			column_sql.append("expnum bigint")
			column_sql.append("tilename varchar(24)")
		for col, val in outputs.items():
			if main and (col=="identifier"):
				column = "identifier BIGINT PRIMARY KEY"
			elif type(val) in (int,long,np.int32,np.int64):
				column = "{0} bigint".format(col)
			elif type(val) in (float,np.float32,np.float64):
				column = "{0} DOUBLE PRECISION".format(col)
			elif type(val)==str:
				column = "{0} varchar(24)".format(col)
			else:
				raise I3StructuralError("Unknown data type %s=%s  (%s)"%(col, val, type(val)))
			column_sql.append(column)
		self.create_db(table_name, column_sql)
		

	def write_row(self, main, epochs):
		if not self.started:
			self.main_columns = main.keys()
			self.epoch_columns = epochs[0].keys()
			self.init_cat(self.main_name, main, main=True)
			self.init_cat(self.epoch_name, epochs[0], main=False)
			self.started = True

		cursor = self.connection.cursor()
		#vals = ",".join(str(main[col]) for col in self.main_columns)
		vals = []
		for col in self.main_columns:
			val = main[col]
			if isinstance(val, np.float) and not np.isfinite(val):
				val="'NaN'"
			elif isinstance(val, basestring):
				val = repr(val)
			else:
				val=str(val)
			vals.append(val)
		vals=','.join(vals)
		sql = "insert into {0} values ({1})".format(self.main_name,vals)
		try:
			cursor.execute(sql)
		except Exception as error:
			print "ERROR with SQL:", sql
			print error
			self.connection.commit()
			return
		main_identifier = str(main['identifier'])
		main_tilename = main.get('tilename', 'Unknown')
		for i,epoch in enumerate(epochs):
			vals = [main_identifier, str(i), repr(main_tilename)]
			for col in self.epoch_columns:
				val = epoch[col]
				if isinstance(val, np.float) and not np.isfinite(val):
					val="'NaN'"
				elif isinstance(val, basestring):
					val = repr(val)
				else:
					val=str(val)
				vals.append(val)
			vals = ','.join(vals)
			sql = "insert into {0} values ({1})".format(self.epoch_name, vals)
			try:
				cursor.execute(sql)
			except:
				print "ERROR with epoch SQL:", sql
				self.connection.commit()
				return
		self.connection.commit()
		cursor.close()

class MPIDatabaseOutput(DatabaseOutput):
	def __init__(self, comm, base_name,
			hostname=default_options.database_host, database=default_options.database_name
			):
		self.comm=comm
		self.rank=comm.Get_rank()
		self.size=comm.Get_size()
		if self.rank==0:
			super(MPIDatabaseOutput,self).__init__(base_name, hostname, database)
		else:
			self.cache = []

	def write_row(self, main, epochs):
		if self.rank==0:
			super(MPIDatabaseOutput, self).write_row(main, epochs)
		else:
			self.cache.append([main,epochs])
			

	def flush(self):
		#send all the tasks at once now to hugely reduce the number
		#of messages being sent
		if self.rank!=0:
			self.comm.send(self.cache, dest=0, tag=self.rank)
			self.cache = []

class MPITextOutput(TextOutput):
	def __init__(self, comm,
		     main_file, epoch_file,
		     main_header=[], epoch_header=[]):
		self.comm=comm
		self.rank=comm.Get_rank()
		self.size=comm.Get_size()
		self.cache = []
		if self.rank==0:
			super(MPITextOutput,self).__init__(main_file, epoch_file,
							   main_header=main_header, epoch_header=epoch_header)
			
	def flush(self):
		#send all the tasks at once now to hugely reduce the number
		#of messages being sent
		if self.rank!=0:
			self.comm.send(self.cache, dest=0, tag=self.rank)
			self.cache = []

	def write_row(self, main, epochs):
		if self.rank==0:
			super(MPITextOutput, self).write_row(main, epochs)
			self.main_cat.flush()
			self.epoch_cat.flush()
		else:
			self.cache.append([main,epochs])
	

