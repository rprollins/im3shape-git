import numpy as np
import math
import sys
from . import common
from .common import *
from . import structs
from . import lib


try:
    import galsim
except:
    pass

from .i3object import I3Object


class Image(I3Object):
    def __init__(self, *args, **kwargs):
        """ """
        super(Image, self).__init__()
        if len(args)==1:
            arg = args[0]
            if isinstance(arg, structs.Image_struct_p):
                self._struct = arg
            elif isinstance(arg, np.ndarray):
                assert arg.ndim==2, "Only 2D arrays can become images"
                nx, ny = arg.shape
                self._struct = lib.i3_image_create(nx, ny)
                self.set_from_array(arg)
            elif isinstance(arg, Image):
                # there is a better way of doing this.
                nx, ny = arg.nx, arg.ny
                self._struct = arg._copy()
            else:
                raise ValueError("Invalid way of creating image: %s" % repr(arg))
        elif len(args)==2:
            nx, ny = args
            self._struct = lib.i3_image_create(nx, ny)
            self.zero()
        elif len(args)==3:
            nx, ny, data = args
            self._struct = lib.i3_image_create(nx, ny)
            if isinstance(data, float):
                self.fill(data)
            elif isinstance(data, np.ndarray):
                self.set_from_array(data)
            else:
                ValueError("Invalid way of filling image: %r" % data)
        else:
            raise ValueError("Invalid way of creating image")

        self.own = kwargs.get("own", True)
        if not self._struct:
            raise MemoryError("Out of memory when creating image")

    def __del__(self):
        if self.own:
            try:
                lib.i3_image_destroy(self._struct)
            except:
                pass

    def set_from_array(self, data):
        if data.shape != (self.nx, self.ny):
            raise ValueError("Cannot set image data from wrongly size array (image:%dx%d, array:%dx%d)"%(data.shape[0], data.shape[1], self.nx, self.ny))
        data_typed = data.flatten().astype(py_i3_flt)
        ptr = data_typed.ctypes.data_as(c_i3_flt_p)
        lib.i3_image_copy_from_pointer(self, ptr)

    def array(self):
        """ 
        Return a copy of the image as an array.
        
        The reason we copy rather than just provide a reference
        is that otherwise the memory is owned by the Image object's 
        C structure instead of the array.  So when the former is deleted
        the latter points to invalid memory.

        """
        return np.ctypeslib.as_array(self.data, (self.ny, self.nx)).copy()

    def rotated90(self):
        rot = lib.i3_image_rotate90(self)
        # rot is a C struct
        img = Image(rot)
        #img is the python instance
        return img

    def show(self, log=False, **kwargs):
        try:
            import pylab
        except ImportError:
            raise RuntimeError("Need pylab installed to show images")
        arr = self.array()
        if log:
            arr = np.log10(arr)
        interpolation = kwargs.pop('interpolation', 'nearest')
        origin = kwargs.pop('origin', 'upper')
        pylab.imshow(arr, interpolation=interpolation, origin=origin)

    def moments(self, as_tuple=False):
        m = structs.Moments_struct()
        lib.i3_image_compute_moments(self, ct.byref(m))
        if as_tuple:
            return m.as_tuple()
        else:
            return m

    def weighted_moments(self, weight_radius=10., iterations=10, as_tuple=False):
        m = structs.Moments_struct()
        lib.i3_image_compute_weighted_moments(self, weight_radius, iterations, ct.byref(m))
        if as_tuple:
            return m.as_tuple()
        else:
            return m

    def rotflip_permute(self, permutation_id):
        """ 
        @ brief Rotate/flip an image according to a permutation number between 0 and 7
        """
        output_array=self.array()

        # mirror flip it if permutation id is odd
        if (permutation_id%2>0):
            output_array=numpy.fliplr(output_array)

        # rotate it floor(permutation_id/2) times
        number_of_times_to_rotate = math.floor(permutation_id/2)
        for irot in numpy.arange(number_of_times_to_rotate):
            output_array=numpy.rot90(output_array)

        return Image(output_array)


    fill = lib.i3_image_fill
    scale = lib.i3_image_scale
    add_const = lib.i3_image_addconst
    subtract_background = lib.i3_image_subtract_background
    max = lib.i3_image_max
    min = lib.i3_image_min
    sum = lib.i3_image_sum
    norm = lib.i3_image_norm
    mean = lib.i3_image_mean
    stdev = lib.i3_image_stdev
    zero = lib.i3_image_zero
    save_fits = lib.i3_image_save_fits
    add_white_noise = lib.i3_image_add_white_noise
    shift_center=lib.i3_image_shift_center
    _add_sersic=lib.i3_add_real_space_sersic
    _add_trunc_sersic=lib.i3_add_real_space_sersic_truncated_radius
    _add_up_sersic=lib.i3_add_real_space_sersic_upsample_central
    _add_trunc_up_sersic=lib.i3_add_real_space_sersic_truncated_radius_upsample_central
    _add_moffat=lib.i3_image_add_moffat
    _add_gaussian=lib.i3_add_real_space_gaussian
    _copy=lib.i3_image_copy

    def add_gaussian(self, ab, e1, e2, A, x0, y0):
        #This thin wrapper is here to expose the param names in python
        self._add_gaussian(ab, e1, e2, A, x0, y0)

    def add_sersic(self, ab, e1, e2, A, x0, y0, n, truncation=0, central_size=0, central_upsampling=1):
        if truncation==0:
            if central_size==0 or central_upsampling<1:
                self._add_sersic(ab, e1, e2, A, x0, y0, n)
            else:
                self._add_up_sersic(ab, e1, e2, A, x0, y0, n, central_size, central_upsampling)
        else:
            if central_size==0 or central_upsampling<1:
                self._add_trunc_sersic(ab, e1, e2, A, x0, y0, n, truncation)
            else:
                 self._add_trunc_up_sersic(ab, e1, e2, A, x0, y0, n, truncation, central_size, central_upsampling)

    def add_moffat(self, radius, e1, e2, x0, y0, beta):
        self._add_moffat(x0, y0, radius, e1, e2, beta)

    @classmethod
    def make_great10_psf(cls, nx, ny, beta, fwhm, e1, e2, options):
        N_pix = nx
        N_sub = options.upsampling
        N_pad = options.padding
        N_all = (N_pix + N_pad) * N_sub
        truncation = options.psf_truncation_pixels
    
        if options.airy_psf: # defined in i3_psf.h
            psf_type = 1     # GREAT10_PSF_TYPE_AIRY
        else:
            psf_type = 0      # GREAT10_PSF_TYPE_MOFFAT

        img = lib.i3_make_great10_psf(N_pix, N_sub, N_pad, beta, fwhm, e1, e2, truncation, psf_type)        
        return cls(img)

    @classmethod
    def create_sersics_model_image_with_moffat_parameters(cls, sersics_params, moffatPSF_struct, options):

        # create model image
        model_image = cls(options.stamp_size, options.stamp_size)

        lib.i3_sersics_model_image_with_moffat( ct.byref(sersics_params), options,
                                                ct.byref(moffatPSF_struct), model_image )

        return model_image

    @classmethod
    def create_sersics_model_image_with_psf_image(cls, sersics_params, psf_image, options):

        # create model image
        model_image = cls(options.stamp_size, options.stamp_size)

        lib.i3_sersics_model_image_with_psf_image( ct.pointer(sersics_params), options,
                                                   psf_image, model_image )

        return model_image

    @classmethod
    def from_GSObject(self, galsim_object, pixel_scale): 
        try:
            galsim
        except NameError:
            raise RuntimeError("Could not import galsim")
        canvas = galsim.ImageD(self.nx, self.ny)
        obj_image = galsim_object.draw(image=canvas, dx=pixel_scale)
        obj_array = obj_image.array.astype(py_i3_flt)        
        return cls(obj_array)

    def set_from_GSObject(self, galsim_object, pixel_scale):
        try:
            galsim
        except NameError:
            raise RuntimeError("Could not import galsim")
        canvas = galsim.ImageD(self.nx, self.ny)
        obj_image = galsim_object.draw(image=canvas, dx=pixel_scale)
        obj_array = obj_image.array.astype(py_i3_flt)
        self.set_from_array(obj_array)

    def copy(self):
        return self.__class__(self._copy())

