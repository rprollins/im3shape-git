##############################################################################################    
#BS residual removal functions    
    
def plot_fov(x,y,psfstat,
             saveto = None,
             label = None, xlabel='arcsec',ylabel='arcsec', title=None,
             vmin=-0.01, vmax= 0.01, fov_min = -4000, fov_max = 4000, 
             scatterplot=False,s=2.):
                 
    import matplotlib.pyplot as plt
    
    fig,ax = plt.subplots()
    if scatterplot:
        im = ax.scatter(x,y,c=psfstat,cmap = plt.cm.bwr,vmin=vmin, vmax=vmax,lw=0,s=s)
    else:    
        im = ax.imshow(psfstat.T,vmin=vmin,vmax=vmax,cmap=plt.cm.bwr,origin='lower',interpolation='None',
                   extent=(x.min(),x.max(),y.min(),y.max()))
                                 
    ax.set_aspect('equal', 'box')
    cb = plt.colorbar(im)
    if label is not None: cb.set_label(label,fontsize=20)
    ax.set_xlim(fov_min,fov_max)
    ax.set_ylim(fov_min,fov_max)
    ax.set_xlabel(xlabel,fontsize=18)
    ax.set_ylabel(ylabel,fontsize=18)
    ax.set_title(title,fontsize=18)
    
    if saveto is not None: fig.savefig(saveto,format='png',dpi=600)
        
    return fig,ax

def bin_spline_fov_residuals(xfov,yfov,psfstat, 
                             nbins=400,nbins_fine=2e3,fov_min=-4000,fov_max=4000, spline_order=3,
                             do_plots = False, estat='', saveroot = None, plot_mask = False,vmin=-0.01,vmax=0.01,
                             labels = {'de1': r'$\delta e_1$', 'de2': r'$\delta e_2$', 'ds': r'$\delta s$' }):
                                 
    """
    averages the psf ellipticity/residual/size on a rectangular grid in the fov and splines it
    returns: spline (can be evaluated with psfstat_interp.ev(x,y) )
    """                    
                                 
    import numpy as np                             
    from scipy.interpolate import RectBivariateSpline #interp2d,
    from scipy.stats import binned_statistic_2d
    
    print('spline order = ', spline_order)
    print('nbins =', nbins)
    
    saveto = None
    
    xbins = np.linspace(fov_min,fov_max,nbins)
    ybins = np.linspace(fov_min,fov_max,nbins)    
    psfstat_binned, xedges, yedges, binnumber = binned_statistic_2d(xfov, yfov, values=psfstat, 
                                                               statistic='mean', bins=[xbins,ybins])
    xbins_centre = (xedges[:-1] + xedges[1:])/2.
    ybins_centre = (yedges[:-1] + yedges[1:])/2.
        
    if do_plots: 
        if saveroot is not None: saveto = saveroot+estat+'_binned.png'
        fig,ax = plot_fov(xbins_centre,ybins_centre,psfstat_binned, label=labels[estat], title='binned residuals',
                          saveto = saveto,vmin=vmin,vmax=vmax)
    
    #mask chip boundaries
    mask = np.isnan(psfstat_binned)
    if plot_mask:
        if saveroot is not None: saveto = saveroot+estat+'_mask.png'
        fig,ax = plot_fov(xbins_centre,ybins_centre,mask,vmin=0.,vmax=1.,label='mask',saveto = saveto)
        #checked that there are no holes within the chips (could happen if bins to fine)

    #tried masked array to avoid issues with spline at chip boundaries, but doesn't help
#    psfstat_binned = np.ma.masked_array(psfstat_binned,mask)
    #instead replace nans with 0. probably a bit lame, but should be OK because we don't need an estimate in the gaps
    psfstat_binned[mask] =0.
    
    #spline: RectBivariateSpline is faster than interp2d, and allows evaluation at scattered values
#    psfstat_interp = interp2d(xbins_centre,ybins_centre,psfstat_binned,kind='cubic')
    psfstat_interp = RectBivariateSpline(xbins_centre,ybins_centre,psfstat_binned,kx=spline_order,ky=spline_order)
        
    if do_plots: 
        xfine = np.linspace(fov_min,fov_max,nbins_fine)
        yfine = np.linspace(fov_min,fov_max,nbins_fine)
        psfstat_splined = psfstat_interp(xfine,yfine)    
        if saveroot is not None: saveto = saveroot+estat+'_splined.png'
        fig,ax = plot_fov(xfine,yfine,psfstat_splined, label = labels[estat],title='splined model',
                          saveto = saveto,vmin=vmin,vmax=vmax)
           
    if do_plots: #regularly binned version of corrected residual (this is faster than hexbin)
        psfstat_corr = psfstat - psfstat_interp.ev(xfov,yfov) 
        psfstat_corr_binned, xedges, yedges, binnumber = binned_statistic_2d(xfov, yfov, values=psfstat_corr, 
                                                                             statistic='mean', bins=[xbins,ybins])
        if saveroot is not None: saveto = saveroot+estat+'_corr_binned.png'
        fig,ax = plot_fov(xbins_centre,ybins_centre,psfstat_corr_binned, label=labels[estat], title = 'splined model subtracted',
                          saveto = saveto,vmin=vmin,vmax=vmax)
                       
    return psfstat_interp
    
def correct_splined_residuals(xfov,yfov,de1,de2,nbins=400,spline_order=3):

    print('binning/splining de1')
    de1_interp = bin_spline_fov_residuals(xfov,yfov,de1,nbins=nbins,spline_order=spline_order)#,
#                                        do_plots=True,saveroot='psfresidtest_nbins'+str(nbins),estat='de1')
    print('binning/splining de2')
    de2_interp = bin_spline_fov_residuals(xfov,yfov,de2,nbins=nbins,spline_order=spline_order)#,
#                                        do_plots=True,saveroot='psfresidtest_nbins'+str(nbins),estat='de2')
    
    # spline correction onto position of stars to compute rho stats. 
    # for actual correction of shear measurement use galaxy fov positions here                      
    de1_xy = de1_interp.ev(xfov,yfov)
    de2_xy = de2_interp.ev(xfov,yfov)        
    de1_corr = de1 - de1_xy
    de2_corr = de2 - de2_xy
    
    
    if True: #testing 
        from astropy.table import Table
        from scipy.stats import pearsonr
        
        tab = Table(data=[de1,de2,de1_xy,de2_xy],names=['de1','de2','de1_xy','de2_xy'])
        #outfile = '/data/bs538/y1a1-v02/de1de2_correction_nbins'+str(nbins)+'.fits'
        outfile = 'de1de2_correction_nbins'+str(nbins)+'.fits'
        print("saving to {}".format(outfile))
        tab.write(outfile,overwrite=True) 
    
        #check correlation between original and splined
        r1,p1 = pearsonr(de1,de1_xy)
        print('de1: (r,p) = ',r1,p1)
        r2,p2 = pearsonr(de2,de2_xy)
        print('de1: (r,p) = ',r2,p2)
        
        #sys.exit('stopping after removing residuals')
        

    return de1_corr,de2_corr

#BS end
#########################################

def measure_rho(data, max_sep, tag=None, prefix='', use_xy=False):
    """Compute the rho statistics
    """
    import treecorr

    e1 = data[prefix+'e1']
    e2 = data[prefix+'e2']
    s = data[prefix+'size']
    p_e1 = data['psfex_e1']
    p_e2 = data['psfex_e2']
    p_s = data['psfex_size']

    de1 = e1-p_e1
    de2 = e2-p_e2
    
    ############################################

    if True: #BS hack, removing residuals
        xfov = data['fov_x']
        yfov = data['fov_y']
        print('BS: correcting de1 and de2')
        print('do we also want to correct the PSF size?')
        nbins = 6400
        spline_order = 5 
        de1,de2 = correct_splined_residuals(xfov,yfov,de1,de2, nbins=nbins, spline_order=spline_order)
        
    ############################################


def make_saved_spline(filename, output_file, nbins=6400, spline_order=5, prefix=''):
    from astropy.io import fits
    import cPickle
    print("Loading catalog")

    data = fits.getdata(filename)

    e1 = data[prefix+'e1']
    e2 = data[prefix+'e2']
    p_e1 = data['psfex_e1']
    p_e2 = data['psfex_e2']

    xfov = data['fov_x']
    yfov = data['fov_y']

    # s = data[prefix+'size']
    # p_s = data['psfex_size']

    de1 = e1-p_e1
    de2 = e2-p_e2

    print("Running delta e1")
    de1_interp = bin_spline_fov_residuals(xfov,yfov,de1,nbins=nbins,spline_order=spline_order)
    print("Running delta e2")
    de2_interp = bin_spline_fov_residuals(xfov,yfov,de2,nbins=nbins,spline_order=spline_order)

    saved_splines = [de1_interp, de2_interp]
    print("Saving splines")

    cPickle.dump(saved_splines, open(output_file, "w"))


if __name__ == '__main__':
    filename = "psf_y1a1-v02_bstest.fits"
    output_file = "psf_y1a1-v02_soergel_fit_6400.pickle"
    print("Low resolution settings!")
    make_saved_spline(filename, output_file, nbins=6400, spline_order=5)
