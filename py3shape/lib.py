from .common import *
from .structs import *


def load_from_library(library_functions, name, restype, *argtypes):
    function = getattr(libim3, name)
    function.restype = restype
    function.argtypes = argtypes
    library_functions[name] = function

_funcs = {}
load_from_library(_funcs, 'i3_image_create',
    Image_struct_p,
    ct.c_ulong, ct.c_ulong)

load_from_library(_funcs, 'i3_image_destroy', 
    None, 
    Image_struct_p)

load_from_library(_funcs, 'i3_image_fill', 
    None, 
    Image_struct_p, c_i3_flt)

load_from_library(_funcs, 'i3_image_scale', 
    None, 
    Image_struct_p, c_i3_flt)

load_from_library(_funcs, 'i3_image_addconst',
    None, 
    Image_struct_p, c_i3_flt)

load_from_library(_funcs, 'i3_image_subtract_background',
    c_i3_flt, 
    Image_struct_p)

load_from_library(_funcs, 'i3_image_add_white_noise',
    None, 
    Image_struct_p, c_i3_flt)

load_from_library(_funcs, 'i3_image_min',
    c_i3_flt, 
    Image_struct_p)

load_from_library(_funcs, 'i3_image_max',
    c_i3_flt, 
    Image_struct_p)

load_from_library(_funcs, 'i3_image_zero',
    None, 
    Image_struct_p)

load_from_library(_funcs, 'i3_image_sum',
    c_i3_flt, 
    Image_struct_p)

load_from_library(_funcs, 'i3_image_norm',
    c_i3_flt, 
    Image_struct_p)

load_from_library(_funcs, 'i3_image_mean',
    c_i3_flt, 
    Image_struct_p)

load_from_library(_funcs, 'i3_image_stdev',
    c_i3_flt, 
    Image_struct_p)

load_from_library(_funcs, 'i3_image_rotate90',
    Image_struct_p, 
    Image_struct_p)

load_from_library(_funcs, 'i3_image_shift_center',
    None, 
    Image_struct_p,ct.c_int,ct.c_int)

load_from_library(_funcs, 'i3_image_save_fits',
    ct.c_int, 
    Image_struct_p,ct.c_char_p)

load_from_library(_funcs, 'i3_image_copy_from_pointer',
    None, 
    Image_struct_p, c_i3_flt_p)

load_from_library(_funcs, 'i3_image_compute_moments',
    None, 
    Image_struct_p, Moments_struct_p)

load_from_library(_funcs, 'i3_image_compute_weighted_moments',
    None, 
    Image_struct_p, c_i3_flt, ct.c_int, Moments_struct_p)

load_from_library(_funcs, 'i3_image_shift_center',
    None, 
    Image_struct_p, ct.c_int, ct.c_int)

load_from_library(_funcs, 'i3_image_copy',
    Image_struct_p, 
    Image_struct_p)

load_from_library(_funcs, 'i3_add_real_space_sersic',
    None, 
    Image_struct_p, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt)

load_from_library(_funcs, 'i3_add_real_space_sersic_truncated_radius',
    None, 
    Image_struct_p, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt)

load_from_library(_funcs, 'i3_add_real_space_sersic_truncated_radius_upsample_central',
    None, 
    Image_struct_p, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, ct.c_int, ct.c_int)


load_from_library(_funcs, 'i3_add_real_space_sersic_upsample_central',
    None, 
    Image_struct_p, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, ct.c_int, ct.c_int)

load_from_library(_funcs, 'i3_image_add_moffat',
    None, 
    Image_struct_p, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt)

load_from_library(_funcs, 'i3_image_add_truncated_moffat',
    None, 
    Image_struct_p, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt)

load_from_library(_funcs, 'i3_add_real_space_gaussian',
    None, 
    Image_struct_p, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt)


load_from_library(_funcs, 'i3_options_default',
    Options_struct_p,
    )

load_from_library(_funcs, 'i3_options_destroy',
    None,
    Options_struct_p,
    )

load_from_library(_funcs, 'i3_options_read',
    None,
    Options_struct_p, ct.c_char_p
    )

load_from_library(_funcs, 'i3_options_printf',
    None,
    Options_struct_p
    )

load_from_library(_funcs, 'i3_options_save',
    None,
    Options_struct_p, ct.c_char_p
    )

load_from_library(_funcs, 'i3_options_read',
    None,
    Options_struct_p, ct.c_char_p
    )


load_from_library(_funcs, 'i3_options_handle_name_value_body',
    ct.c_int,
    Options_struct_p, ct.c_char_p, ct.c_char_p, ct.c_bool, ct.POINTER(ct.c_int)
    )


load_from_library(_funcs, 'i3_analyze_wrapper',
    None,
    Image_struct_p, Image_struct_p, Image_struct_p, Image_struct_p, Image_struct_p,
        Options_struct_p, ct.c_long, c_i3_flt, c_i3_flt, Results_struct_p, ct.c_int
    )

load_from_library(_funcs, 'i3_analyze_exposures_wrapper',
    None,
    ct.c_int, Image_struct_pp, Image_struct_pp, Image_struct_pp, Image_struct_p, 
        Transform_struct_p, ct.c_char_p, Options_struct_p, ct.c_long, c_i3_flt, c_i3_flt, Results_struct_p, ct.c_int
    )



load_from_library(_funcs, 'i3_gsl_init_rng',
    None,

    )

load_from_library(_funcs, 'i3_make_great10_psf',
    Image_struct_p,
    ct.c_int, ct.c_int, ct.c_int, c_i3_flt, c_i3_flt, c_i3_flt, c_i3_flt, 
        c_i3_flt, ct.c_int
    )

#void i3_sersics_model_image_with_moffat(i3_sersics_parameter_set * p, i3_options * options, i3_moffat_psf * psf_func, i3_image * model_image);

#void i3_sersics_model_image_with_psf_image(i3_sersics_parameter_set * p, i3_options * options, i3_image * psf_image, i3_image * model_image);

load_from_library(_funcs, 'i3_sersics_model_image_with_moffat',
    None,
    sersics_parameter_set_p, Options_struct_p, MoffatPSF_struct_p, Image_struct_p
    )

load_from_library(_funcs, 'i3_sersics_model_image_with_psf_image',
    None,
    sersics_parameter_set_p, Options_struct_p, Image_struct_p, Image_struct_p
    )

load_from_library(_funcs, 'i3_model_create',
    Model_struct_p,
    ct.c_char_p, Options_struct_p
    )

load_from_library(_funcs, 'i3_model_posterior',
    c_i3_flt,
    Model_struct_p, Image_struct_p, ParameterSet_struct_p, Dataset_struct_p
    )

load_from_library(_funcs, 'i3_model_likelihood',
    c_i3_flt,
    Model_struct_p, Image_struct_p, ParameterSet_struct_p, Dataset_struct_p
    )


load_from_library(_funcs, 'i3_build_dataset_with_psf_image',
    Dataset_struct_p,
    Options_struct_p, ct.c_long, Image_struct_p, Image_struct_p, Image_struct_p
    )

load_from_library(_funcs, 'i3_model_option_starts',
    ParameterSet_struct_p,
    Model_struct_p, Options_struct_p
    )

load_from_library(_funcs, 'i3_model_prior',
    c_i3_flt, 
    Model_struct_p, ParameterSet_struct_p
    )

load_from_library(_funcs, 'i3_model_input_varied_parameters',
    None,
    Model_struct_p, c_i3_flt_p, ParameterSet_struct_p, ParameterSet_struct_p
    )

load_from_library(_funcs, 'i3_model_number_varied_params',
    ct.c_int,
    Model_struct_p
    )

load_from_library(_funcs, 'i3_model_prior_violations',
    ct.c_bool,
    Model_struct_p, ParameterSet_struct_p, FILE_p)

load_from_library(_funcs, 'i3_model_extract_varied_parameters',
    None,
    Model_struct_p, ParameterSet_struct_p, c_i3_flt_p)

load_from_library(_funcs, 'i3_get_version',
    ct.c_char_p,
    )

load_from_library(_funcs, 'i3_sersic_total_flux',
    c_i3_flt,
    c_i3_flt, c_i3_flt, c_i3_flt
    )


load_from_library(_funcs, 'i3_sersic_total_flux',
    c_i3_flt,
    c_i3_flt, c_i3_flt, c_i3_flt
    )

load_from_library(_funcs, 'i3_sersics_beermat_inplace',
    None,
    sersics_parameter_set_p, Options_struct_p
    )


try:
    load_from_library(_funcs, 'i3_lapack_path_check',
        ct.c_int,
        )
except AttributeError:
    pass



for name,func in _funcs.items():
    locals()[name] = func
del name, func, _funcs
