import time
import numpy as np
import ctypes
import re
import contextlib

DES_PIXEL_SCALE = 0.27


def ID_from_timestamp():
    ID = ''
    gmt = time.gmtime()

    ID = ''.join(
        [str(getattr(gmt,key)) for key in
            ['tm_year', 'tm_mon', 'tm_mday', 'tm_hour', 'tm_min', 'tm_sec']
        ]
    )
    return int(ID)



def small_round_psf_image(n):
    x,y = np.mgrid[:n,:n]
    z2 = (x-n/2.)**2 + (y-n/2.)**2
    sigma2=1.0
    psf = np.exp(-0.5*z2/2/sigma2)
    psf/=psf.sum()
    return psf


def double_pointer_from_list(L,pointer_type):
    """ Utility ctypes function to go from a list of struct instances to 
    a pointer to pointer/array of pointers (these two are compatible)"""
    n = len(L)
    struct_arr = pointer_type*n
    array = struct_arr()
    for i in xrange(n):
        try:
            array[i] = L[i]
        except TypeError:
            array[i] = L[i]._struct
    return array


def noise_estimate_edge(image, mask):
    """ Using all places with non-zero mask around the edge of
        the image, estimate the noise std dev.
    """
    edge = np.concatenate([image[0,:], image[-1,:],image[1:-1,0],image[1:-1,-1]])
    edge_mask = np.concatenate([mask[0,:], mask[-1,:],mask[1:-1,0],mask[1:-1,-1]])
    edge = edge[edge_mask>0]
    return edge.std()

def weight_scaling_estimate_edge(image, weight):
    edge = np.concatenate([image[0,:], image[-1,:],image[1:-1,0],image[1:-1,-1]])
    edge_weight = np.concatenate([weight[0,:], weight[-1,:],weight[1:-1,0],weight[1:-1,-1]])
    w = edge_weight>0
    edge = edge[w]
    edge_sigma = edge_weight[w]**-0.5
    return (edge/edge_sigma).std()



# taken from ucl_des_shar/utils, copied so we don't have to be dependent on ucl_des_shear in im3shape    
def convert_e_linear_to_quadratic(g1, g2):
    """Convert (a - b)/(a + b) style ellipticities to the (a^2 - b^2)/(a^2 + b^2) convention.

    Returns the converted g1, g2 pair as a tuple.
    """
    denom = 1. + np.sqrt(1. - g1 * g1 - g2 * g2)
    return (g1 / denom, g2 / denom)

# taken from ucl_des_shar/utils, copied so we don't have to be dependent on ucl_des_shear in im3shape    
def convert_e_quadratic_to_linear(e1, e2):
    """Convert (a^2 - b^2)/(a^2 + b^2) style ellipticities to the (a - b)/(a + b) convention.

    Returns the converted e1, e2 pair as a tuple.
    """
    denom = .5 * (1. + e1 * e1 + e2 * e2)
    return (e1 / denom, e2 / denom)

def rescale_weight_edge(image, weight, options):
    scaling=utils.weight_scaling_estimate_edge(image, weight)
    weight *= scaling**-2
    if options.verbosity>1:
        print "Scaling exposure ", scaling



def get_unmasked_flux_fraction(model, weight):
    mask = (weight>0).astype(float)
    unmask_sum = (mask*abs(model)).sum()
    sum = abs(model).sum()
    return unmask_sum/sum




#Stolen from Barney Rowe.
#For now I'm not going to use this but instead just use a small round gaussian
def getPSFExarray(psfex, x_image, y_image, nsidex=32, nsidey=32, upsampling=1, offset=None, return_profile=False, distort_function=None):
    """Return an image of the PSFEx model of the PSF as a np array.

    Arguments
    ---------
    psfex       A galsim.des.PSFEx instance opened using, for example,
                `psfex = galsim.des.DES_PSFEx(psfex_file_name)`.
    x_image     Floating point x position on image [pixels]
    y_image     Floating point y position on image [pixels]

    nsidex      Size of PSF image along x [pixels]
    nsidey      Size of PSF image along y [pixels]
    upsampling  Upsampling (see Zuntz et al 2013)
    distort_function  A function taking the GSObject returned by getPSF and returning a new object to be drawn.  Or None

    Returns a np array with shape (nsidey, nsidex) - note the reversal of y and x to match the
    np internal [y, x] style array ordering.  This is to ensure that `pyfits.writeto()` using the
    ouput array creates FITS-compliant output.
    """
    import galsim
    image = galsim.ImageD(nsidex, nsidey)
    #Note galsim uses 1-offset convention whereas coordinates in meds file are 0-offset:
    x_image_galsim = x_image+1
    y_image_galsim = y_image+1
    psf = psfex.getPSF(galsim.PositionD(x_image_galsim, y_image_galsim))

    if distort_function is not None:
        psf = distort_function(psf)

    psf.drawImage(image, scale=1.0/upsampling, offset=offset, method='no_pixel')
    if return_profile:
        return psf, image.array
    return image.array

def getShapeletPSFarray(des_shapelet, x_image, y_image, nsidex=32, nsidey=32, upsampling=1, offset=None):
    """Return an image of the PSFEx model of the PSF as a np array.

    Arguments
    ---------
    des_shapelet      A galsim.des.DES_Shapelet instance opened using, for example,
                      `des_shapelet = galsim.des.DES_Shapelet(shapelet_file_name)`.
                      Usually stored as *_fitpsf.fits files.
    x_image           Floating point x position on image [pixels]
    y_image           Floating point y position on image [pixels]

    nsidex            Size of PSF image along x [pixels]
    nsidey            Size of PSF image along y [pixels]
    upsampling        Upsampling (see Zuntz et al 2013)

    Returns a np array with shape (nsidey, nsidex) - note the reversal of y and x to match the
    np internal [y, x] style array ordering.  This is to ensure that `pyfits.writeto()` using the
    ouput array creates FITS-compliant output.
    """
    import galsim
    image = galsim.ImageD(nsidex, nsidey)
    psf = des_shapelet.getPSF(galsim.PositionD(x_image, y_image), pixel_scale=1.)
    psf.draw(image, scale=1. / upsampling, offset=offset)
    return image.array


def uberseg(object_id,weight,seg):
    #Object id in seg map should be 
    #First check that expected object is in seg map
    if object_id not in seg:
        raise I3UbersegMissing("Self value {} not found in seg map".format(object_id))
    #First get all indices of all seg map pixels which contain an object i.e. are not equal to zero
    obj_inds = np.where(seg!=0)
    #Then loop through pixels in seg map, check which obj ind it is closest to.
    #If the closest obj ind does not correspond to the target, set this pixel in the weight map to zero.
    uber_weight = np.ones_like(weight)
    for i,row in enumerate(seg):
        for j, element in enumerate(row):
            obj_dists = (i-obj_inds[0])**2 + (j-obj_inds[1])**2
            ind_min=np.argmin(obj_dists)
            if seg[obj_inds[0][ind_min],obj_inds[1][ind_min]] != object_id:
                uber_weight[i,j] = 0.
    weight*=uber_weight
    return uber_weight


tile_pattern = re.compile(r'DES[0-9][0-9][0-9][0-9][+-][0-9][0-9][0-9][0-9]')
tile_band_pattern = re.compile(r'DES[0-9][0-9][0-9][0-9][+-][0-9][0-9][0-9][0-9][_-][ugrizy]')
run_pattern = re.compile(r'[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_DES[0-9][0-9][0-9][0-9][+-][0-9][0-9][0-9][0-9]')
great_des_pattern=re.compile(r"nbc(.)+\.meds\.([0-9][0-9][0-9])\.g([0-9][0-9])\.fits")


def find_tilename(name):
    m = tile_pattern.search(name)
    if m is None:
        return "unknown"
    return m.group()



@contextlib.contextmanager
def temporary_values(container, attribute_syntax=False, **kwargs):
    """
    This is a convenience context manager that temporarily sets either
    some mapping values or some attributes to the values specified in the
    keyword arguments and then sets them back to their original values
    afterwards.

    Example:
    >>> x = {'a':1, 'b':2, 'c':3}
    >>> with temporary_values(x, a=100, b=200):
    >>>     print x['a'] + x['b']
    >>> print x['a'], x['b']

    prints:
    300
    1, 2

    """
    if attribute_syntax:
        original_values = {key:getattr(container,key) for key in kwargs}
        for key,val in kwargs.items():
            setattr(container, key, val)
    else:
        original_values = {key:container[key] for key in kwargs}
        for key,val in kwargs.items():
            container[key] = val

    try:
        yield
    finally:
        if attribute_syntax:
            for key, value in original_values.items():
                setattr(container, key, value)
        else:
            for key,val in original_values.items():
                container[key] = val

def get_image_edge_stats(image, weight):
    mask = (weight>0).astype(float)
    mask[1:-1, 1:-1] = 0.0
    edges = image[mask>0.0]
    return edges.mean(), edges.std()



def get_FWHM(image, fwxm=0.5, upsampling=1, radialProfile=False):
    """
    Computes the FWHM of an i3 image. Per default, it computes the
    radial averaged profile for determining the FWHM. Alternatively,
    it computes the FWHMs of the profiles along the x and y axes in +
    and - direction (total of 4) and returns their average as an
    estimator of the FWHM.    
    """
    # compute weighted moments to get centroid
    moments = image.weighted_moments()

    x0 = np.floor(moments.x0)
    y0 = np.floor(moments.y0)

    # abs here so that we can handle negative models
    profile_x = np.abs(image.array()[int(x0), :])
    profile_y = np.abs(image.array()[:, int(y0)])

    max_val = image.array()[int(x0), int(y0)]
    cut_val = max_val * fwxm

    if radialProfile:
        radii, profile = get_radial_profile(image, x0, y0, fwxm)

        diff = abs(profile - cut_val)
         
        # fhwm code from Tomek
        f1 = 0.
        f2 = 0.
        x1 = 0
        x2 = 0
      
        x1 = np.argmin(diff)
        f1 = profile[x1]
         
        if( f1 < cut_val ):  x2 = x1+1
        else:       x2 = x1-1
        f2 = profile[x2];
         
        a = (f1-f2)/(radii[x1] - radii[x2])
        b = f1 - a*radii[x1];
        x3 = (cut_val - b)/a;
         
        fwhm = (2.* x3) / upsampling

    else:
        fwhms = []
        for i in range(4):
            if i == 0:
                profile = profile_x[int(y0)::]
                dc0 = int(x0) - x0
            if i == 1:
                profile = profile_x[0:int(y0)+1][::-1]
                dc0 = -int(x0) + x0
            if i == 2:
                profile = profile_y[int(x0)::]
                dc0 = int(y0) - y0
            if i == 3:
                profile = profile_y[0:int(x0)+1][::-1]
                dc0 = -int(y0) + y0
         
            diff = abs(profile - cut_val)
         
            # fhwm code from Tomek
            f1 = 0.
            f2 = 0.
            x1 = 0
            x2 = 0
            
            x1 = np.argmin(diff)
            f1 = profile[x1]
         
            if( f1 < cut_val ):  x2 = x1+1
            else:       x2 = x1-1
            f2 = profile[x2];
         
            a = (f1-f2)/(x1 - x2)
            b = f1 - a*x1;
            x3 = (cut_val - b)/a;
         
            fwhms.append(2.* (dc0 + x3))

        fwhm =  np.mean(np.array(fwhms))/upsampling

    return fwhm


def get_radial_profile(image, x0=None, y0=None, fwxm=None):
    """
    Gives the radially averaged profile. If no center is specified, it
    is assumed that the object is centered. The optional argument fwxm
    can be specified if the radial profile only up to fwxm needs to be
    computed.
    """
    nx = image.array().shape[0]
    ny = image.array().shape[1]

    if x0 is None:
        x0 = nx/2
    if y0 is None:
        y0 = ny/2
    
    # start by determining radii of all pixels from center
    y, x = np.indices((nx, ny))
    rs = np.sqrt((x - x0)**2 + (y - y0)**2)

    ind = np.argsort(rs.flatten()) 
    sim = image.array().flatten()[ind]
    srs = rs.flatten()[ind]
    urs = np.unique(rs)

    if fwxm is None:    
        profile = [np.mean(sim[np.where(srs == r)]) for r in urs]
    else:
        idx = np.where(sim < fwxm * sim.max())[0][0]
        profile = [np.mean(sim[np.where(srs == r)]) for r in urs[0:idx]]      
        urs = urs[0:idx]
        
    return urs, profile




connection = None
def singleton_connection(hostname='scidb1.nersc.gov', database='des_im3shape'):
    global connection
    if connection is None:
        import psycopg2
        import netrc
        try:
            (user, account, password) = netrc.netrc().authenticators(hostname)
        except:
            user=None
            account=None
            password=None
        connection = psycopg2.connect(database=database, user=user, host=hostname, password=password)
    return connection
