"""
The classes in this file implement a specific MPI parallelization scheme that
is suitable for the problem of galaxy shape measurement.

We have a moderate number (~few thousands) of data files. Each data file has
tens of thousands of tasks (in this case galaxies) in it. Each task can be
processed independently, but we would like to avoid accessing a single file
across lots of different nodes of the computer, since there may be file system
considerations associated with that.  

So we want to split the processes into gangs, and have each gang look at a file.
Within a file the time taken by a single task can vary quite a bit, but there are
enough of them that it should approximately average down reasonably well.  But
some files contain tasks that are generally slower than others (more images of 
each galaxy). So we want to assign files to gangs dynamically, and not just assign
them all at the start.

So the model is: a single process decides on assignments, sending them out the 
leaders of each ganga.  The gang leaders distribute them to the rest of the gang,
and also work on them themself.  Once the file is complete the leader reports
this to the overall root process.

As an aide memoire and because I've been listening to lots of Patrick O'Brian
audio books, the naming scheme is based on a naval fleet.

In this model:
 - The global root process is the Admiral. He[*] makes a list of orders and sends them
 one at a time to the Captains. When a Captain has completed the order he requests
 a new one from the admiral, who either sends him the next one or orders him to 
 return to port (all taskes complete).

 - The remainder of the processes are split into a user-defined number of ships.
 The ships are all the same size as far as is possible, except that the first ship
 has one fewer member (because the admiral stays ashore). The root process of each ship
 is called the captain.  Generally, one or more nodes would be assigned to be a single ship.
 
 - The Captain receives an order from the Admiral and broadcasts it to the other sailors on 
 the ship. All the ship's crew, including the Captain, run a user-supplied function on the 
 order. The parallelization within the ship is assumed to be done within the function, 
 which is called with the rank of the sailor and the size of the ship. e.g. for galaxy
 shape measurement the order just contains the file name, and the processes split up 
 the tasks within it according to their rank.
 
 - The sailors carry out the order and when all are complete the results are gathered by the
 Captain (in the galaxy case there aren't actually any results - it is all just None, but
 the real results have been saved to file).

 - When the Captain has all the completed results he reports them back to the Admiral. 
 He will be sent either a new order or told to return to port, and if the latter he relays
 this to the sailors and they all end the loop.


 [*] I've assumed male sailors for this library because all the way through I've been 
 imagining the Royal Navy in the Napoleonic age. All roles in the modern Royal Navy 
 are open to women, including submarines as of 2016, although as far as I can 
 tell there aren't any women admirals yet.


"""


from math import ceil
import os
import collections
import random

IS_OFFICER = 1
NOT_OFFICER = 2

RETURN_TO_PORT = -1



class Mariner(object):
    def __init__(self, verbose):
        super(Mariner,self).__init__()
        from mpi4py import MPI
        self.MPI = MPI
        self.verbose = verbose
        self.is_admiral = issubclass(self.__class__,Admiral)   

    @staticmethod
    def assign_rank(number_ships, navy=None, admiral_class=None, captain_class=None, sailor_class=None, verbose=False):
        #first split up the navy into ships
        #process 0 is not in any ship (the admiral stays on land)
        #all the other processes are in exactly one ship, 
        #evenly split except the first ship has one fewer process (because the admiral
        #is not on a ship)
        #each ship has one captain.
        #all the captains and the admiral are in the fleet

        if admiral_class is None:
            admiral_class = Admiral
        if captain_class is None:
            captain_class = Captain
        if sailor_class is None:
            sailor_class = Sailor

        #If we don't specify then every process is in the navy.
        if navy is None:
            from mpi4py import MPI
            navy = MPI.COMM_WORLD

        #navy, fleet, and ships are mpi_communicator objects
        rank = navy.Get_rank()
        size = navy.Get_size()
        is_admiral = rank==0

        if size<2:
            raise ValueError("Navy size < 2: Not enough sailors")

        #Split up the navy into the admiral (rank 0)
        #and the rest of the fleet (everyone else)
        #These constants split up the 
        IS_ADMIRAL = 1
        NOT_ADMIRAL = 2

        #First, split all the mariners (processes) into two groups,
        #one with just the admiral in, and one with everyone else.
        #Fun fact: in the Napoleonic Wars the rank of admiral (though
        #not the command position) was determined entirely by seniority
        #among post-captains - once you'd been made captain, if you didn't
        #die first you would one day become an Admiral.
        if is_admiral:
            #We don't need to keep track of the admiral's
            #communicator because there is only one pers
            navy.Split(IS_ADMIRAL, rank)
            is_captain = False
        else:
            everyone_else = navy.Split(NOT_ADMIRAL, rank)

            #Split up the rest of the fleet into evenly sized ships.
            #Actually lets just say the first ship has one less sailor,
            #so we will use the navy ranking to do this
            ship_number=navy.Get_rank()//int(ceil(navy.Get_size()*1./number_ships))
            ship = everyone_else.Split(ship_number, everyone_else.Get_rank())
            ship.name = "HMS {}".format(ship_number)
            if verbose:
                print "Mariner {} in ship {}".format(rank, ship.name)

            #One captain per ship!
            is_captain = ship.Get_rank() == 0

        #Generate the fleet, which is all the captains and the admiral
        if (is_captain or is_admiral):
            fleet = navy.Split(IS_OFFICER, rank)
            if is_admiral:
                assert fleet.Get_rank()==0
        else:
            #the remaining enlisted sailors - we don't actually
            #use this communicator because the sailors get their
            #orders from their captains
            navy.Split(NOT_OFFICER, rank)


        if is_admiral:
            role = admiral_class(fleet, verbose)
        elif is_captain:
            role = captain_class(fleet, ship, verbose)
        else:
            role = sailor_class(ship, verbose)

        return role



class Admiral(Mariner):
    # admiral:
    #     - maintains a queue of tasks
    #     - listens for task requests from captain that requests it
    #     - sends the next task (meds file) from the queue to the captain that requested a order
    #     - marks the captain's orders as complete
    def __init__(self, fleet, verbose):
        super(Admiral, self).__init__(verbose)
        self.fleet = fleet
        self.number_ships = fleet.Get_size()-1
        self.captains = range(1, self.number_ships+1)

    def admirals_report(self, order, captain, report):
        "Admiral looks at Captain's collected report and can do something with it if he wants"
        return report

    def perform_duties(self, orders, function):
        #avoid modifying the input orders
        self.orders = orders[:]
        self.assignments = {}
        self.reports = {}

        #for each order, send it to a captain
        #if there are captains left over, send them ashore
        for captain in self.captains:
            if not self.orders:
                self.send_ashore(captain)
            else:
                self.send_order(captain)

        #While we still have orders to complete
        #send them whenever captain requests an order
        while self.orders:
            #Wait for a messaage from a captain
            captain = self.await_report()
            #Send the next order to the captain
            self.send_order(captain)

        #The send_ashore method removes a captain from the list
        #when they are sent ashore
        while self.captains:
            captain = self.await_report()
            self.send_ashore(captain)

        return self.reports
        

    def await_report(self):
        status = self.MPI.Status()
        report = self.fleet.recv(source=self.MPI.ANY_SOURCE, status=status)
        captain = status.source
        order = self.assignments[captain]
        results = self.admirals_report(order, captain, report)
        self.reports[order] = results
        return captain

    def send_ashore(self, captain):
        self.fleet.send(RETURN_TO_PORT, dest=captain)
        self.captains.remove(captain)

    def send_order(self, captain):
        order = self.orders.pop()
        self.fleet.send(order, dest=captain)
        self.assignments[captain] = order




class Captain(Mariner):
    # captain:
    #     - asks the admiral for a order
    #     - receives a meds file name from the admiral
    #     - tells his crew their order, which they divide evenly amongst them
    #     - runs his own part of the order too
    #     - gets told by the crew when they are done
    def __init__(self, fleet, ship, verbose):
        super(Captain, self).__init__(verbose)
        self.fleet = fleet
        self.ship = ship
        self.rank = ship.Get_rank()
        self.crew_size = ship.Get_size()
        assert self.rank == 0

    def captains_report(self, reports):
        "Collate all the sailors reports. Just returns all the reports as-is by default"
        return reports

    def perform_duties(self, orders, function):
        while True:
            #receive an order from the admiral
            order = self.fleet.recv(source=0)
            #relay it to the ship
            self.ship.bcast(order)
            #if we are ordered back to port then end this loop
            if order is RETURN_TO_PORT:
                if self.verbose:
                    print "Captain on ship {} sent ashore".format(self.ship.name)
                break
            if self.verbose:
                print "Ship {} will perform task: {}".format(self.ship.name, order)

            #captain carries out the order too
            report = function(order, self.rank, self.crew_size)
            #and then gather reports from the other sailors, concatenating
            #it with the captains own report
            reports = self.ship.gather(report)
            #Now collate or otherwide process the reports of the sailors
            captains_report = self.captains_report(reports)
            #Now send these to the admiral and continue waiting for next order
            self.fleet.send(captains_report, dest=0)
        


class Sailor(Mariner):
    # sailor:
    #     - get a order from the captain
    #     - run their part of the order
    #     - tell the captain when they are done
    def __init__(self, ship, verbose):
        super(Sailor, self).__init__(verbose)
        self.ship = ship
        self.crew_size = ship.Get_size()
        self.rank = ship.Get_rank()

    def perform_duties(self, orders, function):
        while True:
            #receive the order from the captain
            order = self.ship.bcast(None)
            #if we are going back to port then finish
            if order==RETURN_TO_PORT:
                #print "Sailor {} on ship {} sent ashore".format(self.rank, self.ship.name)
                break
            #carry out the order
            report = function(order, self.rank, self.crew_size)
            #report to the captain
            self.ship.gather(report)




def example():

    def example_function(order, rank, size):
        "Count the number of characters in a sentence"
        words = order.split()
        n = 0
        for i,word in enumerate(words):
            if i%size==rank:
                n += len(word)
        return n

    #These orders are only actually referenced by the admiral
    orders = [
        "Hello my name is John",
        "What's up?",
        "The quick brown fox jumps over the lazy dog",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.",
        "This is the coming of the age of aquarius",
    ]

    number_ships = 4

    class AdmiralCharacterCount(Admiral):
        def admirals_report(self, sentence, captain, report):
            print "Admiral: Captain {} informs me that the number of non-whitespace characters in '{}' is {}".format(captain, sentence, report)
            return report
    class CaptainCharacterCount(Captain):
        def captains_report(self, reports):
            return sum(reports)

    mariner = Mariner.assign_rank(number_ships, admiral_class=AdmiralCharacterCount, captain_class=CaptainCharacterCount, verbose=True)
    reports = mariner.perform_duties(orders, example_function)


if __name__ == '__main__':
    example()
