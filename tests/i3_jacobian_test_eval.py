from numpy import *

exact  = loadtxt('../tmp/i3_jacobian_test_result_000066_000066_exact.dat')
approx = loadtxt('../tmp/i3_jacobian_test_result_000066_000066_approx_cent.dat')

# column in python 
# 20 error at starting point
# 21 error at estimate
# 28 function evaluations
# 29 Jacobian evaluations



print mean((exact[:,21]-approx[:,21]))


