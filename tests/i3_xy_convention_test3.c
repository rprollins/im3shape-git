#include "i3_image.h"
#include "i3_load_data.h"
#include "i3_image_fits.h"
#include "i3_model_tools.h"


int main(int argc, char * argv[]){
	char * output_filename = argv[1];
	int nx = 100;
	int ny = 100;
	i3_image * image = i3_image_create(nx,ny);
	i3_image_zero(image);
	i3_flt x0 = 0;
	i3_flt y0 = 25;
	i3_flt ab = 20.0;
	i3_flt e1 = 0.0;
	i3_flt e2 = 0.0;
	i3_flt A = 1.0;
	i3_flt index = 1.0;
	i3_flt truncation_factor = 1e30;
	i3_add_real_space_gaussian(image, ab, e1, e2, A, x0, y0);
	int status = i3_image_save_fits(image, output_filename);
	printf("%d\n",status);
	return status;
}