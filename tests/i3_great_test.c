#include "i3_great.h"
#include "i3_model_tools.h"
int main(){

	// For reference: these are the e1,e2 from the great08 star set 1 PSF.
	// They are actually defined in i3_great.c
	// #define GREAT08_STAR_SET_1_G1 -0.019
	// #define GREAT08_STAR_SET_1_G2 -0.007
	

	int N=1001;
	int set_number = 1;
	int upsampling = 1; // hope I'm doing this right - SLB20110617

	i3_flt ab_bulge = 3200.0;
	i3_flt ab_disc = 6400.0;
	i3_flt e1 = 0.0;
	i3_flt e2 = 0.0;
	i3_flt A_bulge = 1.0;
	i3_flt A_disc = 2.0;
	i3_flt x0 = N/2.;
	i3_flt y0 = N/2.;
	
	i3_flt sersic_dvc = 4.0;
	i3_flt sersic_exp = 1.0;

	//Generate a galaxy
	i3_image * image = i3_image_create(N,N);
	i3_image_zero(image);
	i3_add_real_space_sersic(image, ab_bulge, e1, e2, A_bulge, x0, y0, sersic_dvc);
	i3_add_real_space_sersic(image, ab_disc, e1, e2, A_disc, x0, y0, sersic_exp);
	i3_image_save_text(image,"great08_galaxy.txt");

	//Generate the Great08 PSF
	i3_image * psf = i3_great08_psf(set_number, N, x0, y0, upsampling);
	i3_image_save_text(psf,"great08_psf.txt");
	
	//Convolution of the two items.
	i3_fourier * psf_kernel = i3_image_fourier(psf);
	i3_image_convolve_fourier(image,psf_kernel);
	i3_image * observed = i3_image_centered(image);
	i3_image_save_text(observed,"great08_convolved.txt");
	return 0;
	
}