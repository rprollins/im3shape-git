#include "i3_image.h"

int main(){

        // First do ellipticity beermat tests
	i3_flt ei_min = -5;
	i3_flt ei_max = 5;
	i3_flt de = 0.01;
	i3_flt BEERMAT = 0.95;
	i3_flt e1_out, e2_out;
	FILE * f1 = fopen("beer1.txt","w");
	FILE * f2 = fopen("beer2.txt","w");
	for (i3_flt e1_in = ei_min; e1_in<= ei_max; e1_in+=de){
		for (i3_flt e2_in = ei_min; e2_in<= ei_max; e2_in+=de){
			i3_e12_to_e12beermat(e1_in, e2_in, BEERMAT, &e1_out, &e2_out);
			fprintf(f1,"%f\t",e1_out);
			fprintf(f2,"%f\t",e2_out);
		}
		fprintf(f1,"\n");
		fprintf(f2,"\n");
	}
	fclose(f1);
	fclose(f2);

	//1D plot
	i3_flt e1_in = 0.0;
	FILE * f = fopen("beer.txt","w");
	for (i3_flt e2_in = ei_min; e2_in<= ei_max; e2_in+=de){
		i3_e12_to_e12beermat(e1_in, e2_in, BEERMAT, &e1_out, &e2_out);
		fprintf(f,"%f\t%f\t%f\n",e2_in,e1_out,e2_out);
	}
	fclose(f);

        // Then do radius beermat tests (ID only)
	i3_flt r_min = -1.;
	i3_flt r_max = 1.;
	i3_flt dr = 0.001;
	i3_flt RPARAM = 0.2;
	i3_flt r_out;
	FILE * f3 = fopen("beer_r.txt", "w");
	for (i3_flt r_in = r_min; r_in <= r_max; r_in += dr){
            i3_r_to_rbeermat(r_in, RPARAM, &r_out);
            fprintf(f3, "%f\t", r_in);
	    fprintf(f3, "%f\t", r_out);
            fprintf(f3, "\n");	
	}
	fclose(f3);

}
