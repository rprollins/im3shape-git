#include "i3_mcmc.h"
#include "i3_load_data.h"
#include "i3_math.h"
#include "i3_model.h"
#include "i3_minimizer.h"
#include "i3_fisher.h"
#include "i3_mcmc.h"
#include "i3_image_fits.h"
#ifdef MPI
#include "mpi.h"
#endif


#include "i3_analyze.h"
#include "i3_model_tools.h"
#include "i3_data_set.h"
#include "i3_great.h"
#include "i3_psf.h"
#include "i3_image_fits.h"
#include "i3_utils.h"
#include "i3_options.h"
#include <time.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>



int main(int argc, char *argv[]){

	if(argc!=4){
		printf("Usage: %s n_sub save_noisy_image[0,1] filename_ini\n",argv[0]);
		return 0;
	}
	
	int n_sub = atoi(argv[1]);
	int save_noisy_image = atoi(argv[2]);
	char * filename_ini = argv[3];

	//Initialize FFTW
	i3_fftw_load_wisdom();
	atexit(i3_fftw_save_wisdom);

	// Build options
	i3_options * options = i3_options_default();
	i3_options_read(options, filename_ini);

	//int n_sub = options->upsampling;
	options->upsampling = n_sub;
	int n_pix = options->stamp_size;
	int n_pad = options->padding;
	int n_all = n_pix + n_pad;
	int n_win = (n_pad+1) * n_sub; // this will be used to iterate over a moving window over the additional pixels, corresponding to checking the xy0 grid outside the central pixel

		
	i3_flt snr = 20000;
	i3_flt noise_sigma = 1e-1;
	
	i3_image * image_noisy;
	if(save_noisy_image==1) image_noisy = i3_image_create(n_pix,n_pix);
	if(save_noisy_image==0) image_noisy = i3_image_load_fits( "img_max_xy0_test_noisy.fits" );
	i3_image * weight = i3_image_create(n_pix,n_pix); 
        i3_image_fill(weight, 1. );
	
	//Create basic information and space for the chosen model (e.g. milestone)
	i3_model * model   = i3_model_create(options->model_name, options);
	

	//Set up the PSF
	i3_moffat_psf psf_form;
	psf_form.beta = 3.;
	psf_form.e1 = 0.;
	psf_form.e2 = 0.;
	psf_form.fwhm = 2.85;
        
	i3_data_set * dataset = i3_build_dataset(options, image_noisy, weight, &psf_form, GREAT10_PSF_TYPE_MOFFAT);

	// Build the model parameters
	i3_milestone_parameter_set * params_gal = i3_model_option_starts(model, options);
	params_gal->radius = 1.2;
	params_gal->radius_ratio = 1.25;
	params_gal->flux_ratio = 0.9999;
	params_gal->bulge_index = 4.0;
	params_gal->disc_index = 1.0;
        params_gal->x0 = n_pix/2+0.;
	params_gal->y0 = n_pix/2-1.51;
	params_gal->e1 = 0.2;
       	params_gal->e2 = 0.;

	if(save_noisy_image==1){

		// Build the image
		i3_milestone_model_image(params_gal,dataset,image_noisy);
		//i3_model_get_image_into(model,params_gal,dataset,image_noisy);
		i3_image_save_fits(image_noisy, "img_max_xy0_test_noiseless.fits");

		// Add noise
		i3_init_rng();
		noise_sigma = i3_snr_to_noise_std( snr, image_noisy );
		i3_image_add_white_noise(image_noisy, noise_sigma);
		i3_image_save_fits(image_noisy, "img_max_xy0_test_noisy.fits");
	
	}

// 	i3_image * image_dvc;
// 	i3_image * image_exp;
// 
// 	image_dvc = i3_image_create(n_all*n_sub,n_all*n_sub);	i3_image_zero(image_dvc);
// 	image_exp = i3_image_create(n_all*n_sub,n_all*n_sub);	i3_image_zero(image_exp);

// for this model we always generate a model in the middle of the postage stamp

	//params_gal->x0 = n_pix/2;
	//params_gal->y0 = n_pix/2;

	//i3_sersics_components( params_gal,dataset,image_dvc,image_exp );  

	//i3_flt logL,bx1,bx2; 
	//i3_sersics_likelihood_in_xy0(image_dvc,image_exp,params_gal,dataset,&logL,&bx1,&bx2);

// run im3shape

// Analyse the test data

	i3_analyze_dataset_method(dataset, model, options, stdout, 1, i3_minimizer_method_praxis);
	//i3_analyze_dataset_method(dataset, model, options, stdout, 1, i3_minimizer_method_levmar);
	

 	printf("true x0 y0 e1 e2 %f %f %f %f \n",params_gal->x0,params_gal->y0,params_gal->e1, params_gal->e2);

	  


}



