close all; clc; clear all;

%%

% models name 
models_id = 'test4';

% precision
precision  = 'float';

% put the truth galaxy id
mid = 39441;

nsub = 5;
n_grid_f = 11;
n_grid_e = 71;


%%
n_grid_ee = n_grid_e*n_grid_e;
n_grid_eef = n_grid_ee *n_grid_f;
%grid_eef_f = n_grid_ee *n_grid_f;


% load the noisy image
%load obs_img.txt; noisy = obs_img(:);
fid_logL   = fopen('~/im3shape/projects/noise_bias_study/i3_likelihood_test_logL.dat','r');
fid_pdf   = fopen('~/im3shape/projects/noise_bias_study/i3_likelihood_test_pdf_full.dat','r');
fid_params  = fopen(['~/im3shape/projects/noise_bias_study/models/models_' models_id '_params.dat'],'r');       

logL = fread(fid_logL, n_grid_eef ,precision);
pdf = fread(fid_pdf, n_grid_eef ,precision);
params_all = fread(fid_params, 3*n_grid_eef ,precision);
params_all = reshape(params_all,[3 n_grid_eef])';
grid_eef_e = params_all(:,1) + i*params_all(:,2);
grid_eef_f = params_all(:,3)/nsub;

grid_f = unique(grid_eef_f);
grid_e = grid_eef_e(grid_eef_f == grid_eef_f(1));

% truth
e_true = grid_eef_e(mid);
fwhm_true = grid_eef_f(mid);

%% get the ML in conditional

% ml
[vm,im] = max(logL);
e_gml = grid_eef_e(im);
fwhm_gml = grid_eef_f(im);
disp('ml solution in both e and fwhm')
disp(num2str([real(e_gml),imag(e_gml)],'e1_gml = \t %2.2f \t e2_gml = \t %2.2f'))
disp(num2str([real(e_true),imag(e_true)],'e1_true = \t %2.2f \t e2_true = \t %2.2f'))
disp(num2str([real(e_true)-real(e_gml),imag(e_true)-imag(e_gml)],'e1_bias = \t %2.2f \t e2_bias = \t %2.2f'))
disp(num2str([fwhm_gml,fwhm_true],'fwhm_ml = \t %2.4f \t fwhm_true = \t %2.4f'))

%% initialise the marginal likelihood
select = grid_eef_f == grid_eef_f(1);
margL = zeros(size(logL(select)));

%% calculate and prev the conditionals
plots = 1;

P = pdf;

for f = grid_f'
    select = grid_eef_f == f;
    selected_e = grid_eef_e(select);
    
    [vm,im] = max(P(select));
    e_cml = selected_e(im);
    margL = margL + P(select);
    
    if(plots == 1)
        scatter(0,0,50,max(P)); hold on;
        %scatter(real(grid_e),imag(grid_e),50,margL,'filled'); hold on; colorbar     
        scatter(real(grid_e),imag(grid_e),50,P(select),'filled'); hold on; colorbar     
        scatter(real(e_true), imag(e_true),100,'pr')
        scatter(real(e_cml), imag(e_cml),100,'xr')
        scatter(real(e_gml), imag(e_gml),100,'+r'); hold on    
        titlestr = num2str([real(e_cml) imag(e_cml) real(e_true) imag(e_true) f fwhm_true],'e_{ml} = %2.4f %2.4f   e_{true} = %2.2f %2.2f    fwhm = %2.4f    fwhm_{true} = %2.4f');
        if(e_gml == e_cml) 
            titlestr = [titlestr '  *'];
        end
        title(titlestr)   
        axis equal
        pause
    end
    
end

%% get the ML in marginal

% ml
[vm,im] = max(margL);
e_mml = grid_e(im);

disp('ml solution in both e with marginalised fwhm')
disp(num2str([real(e_mml),imag(e_mml)],'e1_mml = \t %2.2f \t e2_mml = \t %2.2f'))
disp(num2str([real(e_true),imag(e_true)],'e2_true = \t %2.2f \t e2_true = \t %2.2f'))



%% plot the marginal
    
scatter(0,0,50,max(margL)); hold on;
scatter(real(grid_e),imag(grid_e),50,margL,'filled'); hold on; colorbar
scatter(real(e_true), imag(e_true),100,'pr')
scatter(real(e_gml), imag(e_gml),100,'+r'); hold on
%scatter(real(e_mml), imag(e_mml),100,'xr'); hold on
titlestr = num2str([real(e_mml) imag(e_mml) real(e_true) imag(e_true) f fwhm_true],'e_{ml} = %2.2f %2.2f   e_{true} = %2.2f %2.2f    fwhm = %2.2f    fwhm_{true} = %2.2f');
title(titlestr)
if(e_gml == e_mml) 
    disp('gml and mml are the same')
else
    disp('gml and mml are NOT the same')
end


return
%% read in the marginals to test if matlab and im3 are the same

fid_pdf_marg   = fopen('~/code/ucl_des_shear/im3shape/tests/pdf_marg.dat','r');
pdf_marg = fread(fid_pdf_marg, n_grid_e*n_grid_e ,precision);

figure(1);scatter(real(grid_e),imag(grid_e),50,margL,'filled'); hold on; colorbar
figure(2);scatter(real(grid_e),imag(grid_e),30,pdf_marg,'filled'); hold on; colorbar
figure(3);scatter(real(grid_e),imag(grid_e),50,pdf_marg-margL,'filled'); hold on; colorbar

[vm,im] = max(pdf_marg);
e_m2ml = grid_e(im);
e_m2ml - e_mml

return
%% plot the observed image

obs_img  = load('~/code/ucl_des_shear/im3shape/projects/noise_bias_study/i3_likelihood_test_obs_img.txt');

%% plot 3d

surf(reshape(logL(select),n_grid_e,n_grid_e))

%% make combined plot
clear all;close all;clc

load fig2_panel2; 

subplot(1,2,1)
scatter(0,0,50,max(margL)); hold on;
scatter(real(grid_e),imag(grid_e),50,margL,'filled'); hold on; colorbar
scatter(real(e_true), imag(e_true),200,'pr')
scatter(real(e_gml), imag(e_gml),200,'+r'); hold on
title('high signal to noise')
axis equal
axis([-0.7 0.7 -0.7 0.7])

load fig2_panel1; 
subplot(1,2,2)
scatter(0,0,50,max(margL)); hold on;
scatter(real(grid_e),imag(grid_e),50,margL,'filled'); hold on; colorbar
scatter(real(e_true), imag(e_true),200,'pr')
scatter(real(e_gml), imag(e_gml),200,'+r'); hold on
title('low signal to noise')
axis equal
axis([-0.7 0.7 -0.7 0.7])

 
