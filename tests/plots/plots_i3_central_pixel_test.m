image_id = '005'

i_cpu = fitsread(['image_gal_' image_id '_nopsf_hires_cpu_dvc.fits']);
i_nocpu = fitsread(['image_gal_' image_id '_nopsf_hires_nocpu_dvc.fits']);
i_conv = fitsread(['image_gal_' image_id '_hires_dvc.fits']);

n=0;
n=n+1;figure(n); imagesc(i_cpu); colorbar;
n=n+1;figure(n); imagesc(i_nocpu); colorbar;
n=n+1;figure(n); imagesc((i_cpu-i_nocpu)./i_cpu); colorbar;
n=n+1;figure(n); imagesc(i_conv); colorbar;
n=n+1;figure(n); imagesc(i_cpu <= 0 ); colorbar;
n=n+1;figure(n); imagesc(i_nocpu <= 0 ); colorbar;


