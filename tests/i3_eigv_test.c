#include "i3_math.h"
#include "i3_logging.h"

int main(){


#ifdef USE_LAPACK
	int n = 2;
	double A[n*n];
	double e[n];
	A[0]=0.86819805;  A[1]=-0.31819805;
	A[2]=-0.31819805;  A[3]=0.23180195;
	int status = i3_covmat_eigensystem(A, e, n);

	for (int i=0; i<n; i++){
		for (int j=0; j<n; j++){
			printf("%lf   ", A[i+n*j]);
		}
		printf("\n");
	}
	printf("\neigenvalues: ");
	for (int i=0; i<n; i++){
		printf("%lf   ", e[i]);
	}

	printf("\n\nstatus = %d\n\n",status);

#else
	fprintf(stderr, "Cannot do eigv test withough lapack compiled\n");
#endif

}

//
// covmat =[[ 0.86819805, -0.31819805],[-0.31819805,  0.23180195]])
// want eigvectors (1,0.1)
// eigvals (0.92387953, -0.38268343)
//    and  (0.38268343, 0.92387953)