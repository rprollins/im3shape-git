#include "i3_image.h"
#include "i3_model_tools.h"

int main(){
	int n = 48;
	i3_image * image = i3_image_create(n,n);
	
	i3_flt ab = 25;
	i3_flt e1 = 0.0;
	i3_flt e2 = 0.9;
	i3_flt A = 1.0;
	i3_flt x0 = n/2.;
	i3_flt y0 = n/2.;
	i3_image_zero(image);
	i3_add_real_space_gaussian(image, ab, e1, e2, A,x0, y0);
	i3_image_save_text(image, "ellipse.txt");

	e1 = 0.0;
	e2 = 1.1;
	ab = 500.0;
	i3_image_zero(image);
	i3_add_real_space_gaussian(image, ab, e1, e2, A,x0, y0);
	i3_image_save_text(image, "hyperbola.txt");
	

}