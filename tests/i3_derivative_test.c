#include "i3_model.h"
#include "i3_minimizer.h"

extern int i3_quadratic_test_number_gradient_calls, i3_quadratic_test_number_calls;
int main(){
	i3_options * options = i3_options_default();
	options->minimizer_tolerance = 1.0;
	i3_model * model = i3_model_create("quadratic_test",options);
	i3_quadratic_test_parameter_set * p = malloc(model->nbytes);
	// i3_quadratic_test_parameter_set * pprime = malloc(model->nbytes);
	p->x1 = QUADRATIC_TEST_TRUE_MEAN1;
	p->x2 = QUADRATIC_TEST_TRUE_MEAN2;
	p->x3 = QUADRATIC_TEST_TRUE_MEAN3;
	p->x4 = QUADRATIC_TEST_TRUE_MEAN4;
	
	
	i3_minimizer * minimizer = i3_minimizer_create(model,options);
	i3_quadratic_test_parameter_set * initial = malloc(model->nbytes);
	initial->x1 = 0.5;
	initial->x2 = 0.5;
	initial->x3 = 0.5;
	initial->x4 = 0.5;
	
	i3_data_set * data_set = NULL; /* We do not need a data set because the test likelihood does not use it.*/
	i3_quadratic_test_parameter_set * best = i3_minimizer_with_derivatives(minimizer,initial,data_set);
	
	printf("\nBEST:\n");
	if(best) i3_model_pretty_printf_parameters(model,best);
	else printf("NO FIT\n");
	printf("\n\n");
	printf("Calls to gradient = %d\n",i3_quadratic_test_number_gradient_calls);
	printf("Calls to likelihood = %d\n",i3_quadratic_test_number_calls);
	
	
}

