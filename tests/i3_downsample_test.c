#include "i3_image.h"
#include "i3_model_tools.h"
#include "i3_mcmc.h"
#include "i3_minimizer.h"
#include "i3_psf.h"


int main(){
	i3_fftw_load_wisdom();
	atexit(i3_fftw_save_wisdom);
	i3_init_rng();
	
	int nx_low = 32;
	int ny_low = 32;
	int padding = 0;
	int upsampling = 5;
	
	i3_fourier * fourier = i3_fourier_downsampling_psf(nx_low,ny_low,upsampling,padding);
	i3_image * real_part = i3_fourier_real_part(fourier);
	i3_image * imag_part = i3_fourier_imaginary_part(fourier);
	
	i3_image_save_text(real_part,"downsampler_real.txt");
	i3_image_save_text(imag_part,"downsampler_imag.txt");
	
	i3_fourier * fourier2 = i3_fourier_downsampling_psf2(nx_low,ny_low,upsampling);
	i3_image * real_part2 = i3_fourier_real_part(fourier2);
	i3_image * imag_part2 = i3_fourier_imaginary_part(fourier2);

	i3_image_save_text(real_part2,"downsampler_real2.txt");
	i3_image_save_text(imag_part2,"downsampler_imag2.txt");
	

	i3_image * image = i3_image_create(nx_low*upsampling,ny_low*upsampling);
	i3_image_zero(image);
//	i3_add_real_space_gaussian(40*upsampling,0.0,0.0,1.0,image->nx/2,image->ny/2, image);
	i3_image_add_white_noise(image,1.0);
	i3_image_save_text(image,"image.txt");

	i3_image * lowres = i3_image_downsample(image,nx_low,ny_low);
	i3_image_save_text(lowres,"downsampled1.txt");
	
	i3_image * image2 = i3_image_copy(image);
	i3_image_convolve_fourier(image,fourier);
	i3_image_shift_center(image,nx_low*upsampling/2.,nx_low*upsampling/2.);
	i3_image_convolve_fourier(image2,fourier2);

	i3_image_save_text(image,"downsampled2.txt");
	i3_image_save_text(image2,"downsampled3.txt");
	return 0;

}