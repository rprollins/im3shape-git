#include "i3_global.h"
#include "i3_image.h"
#include "i3_model_tools.h"
#include "i3_math.h"

int main(int argc, char * argv[]){
	
	i3_flt r = 50.0;
	i3_flt s = 4.0;

	if (argc>=2) r = atof(argv[1]);
	if (argc>=3) s = atof(argv[2]);


	i3_flt e1 = 0.5;
	i3_flt e2 = 0.0;
	i3_flt A = 1.0;
	
	i3_flt ab = r*r;
	i3_flt truncation = 2.0;
	
	
	// Test the truncation code.
	i3_flt e_max = i3_fabs(e1)>i3_fabs(e2) ? i3_fabs(e1) : i3_fabs(e2);
	int N_t = r*truncation*2.5*(1./(1-e_max));
	printf("Truncation test:\n");
	printf("Max e = %f.  So size = %d\n",e_max,N_t);
	i3_flt x0_t = N_t/2.;
	i3_flt y0_t = N_t/2.;

	i3_image * image = i3_image_create(N_t,N_t);
	i3_add_real_space_sersic_truncated_radius(image, ab, e1,e2, A, x0_t, y0_t, s, truncation);
	i3_image_save_text(image,"trunc.txt");
	i3_image_destroy(image);
	
	printf("\nFor scale radius r = %.2f, sersic n = %.1f:\n",r,s);
	i3_flt analytic_flux = i3_sersic_flux_to_radius(s, A, ab, sqrt(ab)*truncation);
	printf("Analytic total flux = %f\n",analytic_flux);
	i3_flt summed_flux = i3_image_sum(image);
	printf("Summed flux = %f\n",summed_flux);
	
	printf("\n");
	if (argc==1) printf("You can also run this program with optional arguments to change parameters:\ni3_sersic_flux [scale_radius] [sersic_n]\n");
	
	
}




