#include "i3_mcmc.h"
#include "i3_load_data.h"
#include "i3_math.h"
#include "i3_model.h"
#include "i3_minimizer.h"
#include "i3_fisher.h"
#include "i3_mcmc.h"
#include "i3_utils.h"
#include "i3_great.h"
#include "fitsio.h"
#include "i3_image_fits.h"

i3_data_set * build_dataset(i3_options * options, i3_image * image, i3_image * weight, i3_fourier * kernel){
	i3_data_set * data_set = (i3_data_set*)malloc(sizeof(i3_data_set));
	data_set->image = image;
	data_set->psf_downsampler_kernel = kernel;
	data_set->noise=options->noise_sigma;
	data_set->upsampling=options->upsampling;
	data_set->weight=weight;
	data_set->padding=options->padding;
	i3_dataset_reset_counters(data_set);
	return data_set;
}


i3_image * cut_out_stamp(i3_image * image, int stamp_size)
{
	//Figure out the amount of image to cut
	int cutx = image->nx - stamp_size;
	int cuty = image->ny - stamp_size;

	//If the image is smaller than the stamp then fail.
	if (cutx<0 || cuty<0) return NULL;
	
	//If the image is already at the stamp size then just return a copy of it.
	if (cutx==0 && cuty==0) return i3_image_copy(image);
	i3_image * output = i3_image_copy_part(image,cutx/2,cuty/2,stamp_size,stamp_size);
	if (output->nx!=stamp_size || output->ny!=stamp_size) I3_FATAL("Failed to cut out correct size of stamp (Joe's fault).",5);
	return output;
}



void save_result(i3_model * model, i3_parameter_set * best, i3_image * model_image, i3_image * data, i3_flt like, char * output_dir, FILE * output_file, int identifier, i3_data_set * data_set){

	if (like==BAD_LIKELIHOOD) like = 0.0/0.0;
	//Print the likelihood, followed by the model parameters, all on one line.
	fprintf(output_file,"%d\t%e\t",identifier,like);
	i3_model_fprintf_parameters(model,output_file,best);
	fprintf (output_file, "\t%d",data_set->n_logL_evals);
	fprintf (output_file, "\t%f",data_set->signal_to_noise);
	fprintf (output_file, "\t%f",data_set->amplitude1);
	fprintf (output_file, "\t%f",data_set->amplitude2);
	fprintf(output_file,"\n");
	fflush(output_file);
	
	if (output_dir){
		//Now save the image and the model to file for later perusal.
		char filename[i3_max_string_length];
		snprintf(filename,i3_max_string_length,"%s/model.%d.txt",output_dir,identifier);
		i3_image_save_text(model_image,filename);
	
		snprintf(filename,i3_max_string_length,"%s/image.%d.txt",output_dir,identifier);
		i3_image_save_text(data,filename);
	}
	
}

bool image_already_done(char * output_dir, int identifier){
	char filename[i3_max_string_length];
	snprintf(filename,i3_max_string_length,"%s/model.%d.txt",output_dir,identifier);
	if (!i3_file_exists(filename)) return false;
	snprintf(filename,i3_max_string_length,"%s/image.%d.txt",output_dir,identifier);
	if (!i3_file_exists(filename)) return false;
	return true;
}

void print_result(i3_model * model, i3_parameter_set * best, i3_flt like, i3_data_set * data_set)
{
	//Print the likelihood, followed by the model parameters, all on one line.
	if (like==BAD_LIKELIHOOD) like = 0.0/0.0;
	printf("%f\t",like);
	i3_model_printf_parameters(model,best);
	
	printf ("\t%d",data_set->n_logL_evals);
	printf ("\t%f",data_set->signal_to_noise);
	printf ("\t%f",data_set->amplitude1);
	printf ("\t%f",data_set->amplitude2);
	printf("\n");
	fflush(stdout);
}


void log_callback(i3_mcmc * mcmc ,i3_data_set * data_set,i3_options *options){
	i3_flt L = i3_mcmc_last_like(mcmc);
	printf("%.12e\t",L);
	i3_model_printf_parameters(mcmc->model,i3_mcmc_last_sample(mcmc));
	printf("\n");
}



void analyze_image(i3_image * image, i3_moffat_psf * psf_form, i3_image * weight, i3_model * model, i3_options * options, FILE * output_file, int identifier)
{
	
	int psf_type = options->airy_psf ? GREAT10_PSF_TYPE_AIRY : GREAT10_PSF_TYPE_MOFFAT;
	i3_fourier * kernel = i3_build_great10_kernel(psf_form, image->nx, options->upsampling, options->padding, psf_type);


	// Create a data set from the image.  Use a noise level set in the file.
	i3_data_set * data_set = build_dataset(options,image,weight,kernel);

	//Choose the point from which to start the minimization.
	i3_parameter_set * start = i3_model_option_starts(model, options);		

	//Set up a minimizer object to perform the minimization
	i3_minimizer * minimizer = i3_minimizer_create(model,options);
	
	//Run the minimizer to extract the best fit result and the likelihood
	i3_flt like = BAD_LIKELIHOOD;
	i3_parameter_set * current_best=NULL;
	data_set->signal_to_noise=0.0/0.0;
	i3_flt A1=0.0, A2=0.0;
	int accepted_loops=0;
	for(int i=0;i<options->minimizer_loops;i++){
		//current_best = i3_minimizer_run_praxis(minimizer,start,data_set);
		current_best = i3_minimizer_run_praxis(minimizer,start,data_set);
		bool any_nan = i3_model_any_nan(model,current_best);

		// printf("any nan = %d\n",any_nan);
		if (minimizer->best_like>like  && (!any_nan) ) {
			// printf("LOOP ACCEPTED\n");
			like = minimizer->best_like;
			free(start);
			start = current_best;
			data_set->signal_to_noise = i3_signal_to_noise_model_fitted(image,minimizer->model_image);
			A1 = data_set->amplitude1;
			A2 = data_set->amplitude2;
			accepted_loops++;
		}
		else{
			free(current_best);
			current_best=start;
		}
		
		
		//Call the funtion above toprint out the result
		data_set->amplitude1 = A1;
		data_set->amplitude2 = A2;
		if (options->verbosity>2 && i<options->minimizer_loops-1) print_result(model,current_best,like,data_set);
	}
	i3_model_posterior(model,minimizer->model_image,current_best,data_set);
	
	if (options->verbosity>-1) {print_result(model,current_best,like,data_set); printf("\n");}
	
	// We run an optional MCMC at the end of the optimizer.
	// Right now we just take the best value in its chain,
	// but depending on the outcome of Tomek's work we 
	// may want to change this behaviour.
	if (options->do_mcmc){
		i3_mcmc * mcmc = i3_mcmc_create(model,options);
		i3_mcmc_run(mcmc, current_best, data_set, options);
		if (options->verbosity>2)fprintf(stderr,"Acceptance = %f\n",i3_mcmc_acceptance_rate(mcmc));
		if (options->use_mcmc_mean){
			i3_mcmc_compute_statistics(mcmc,current_best,NULL);
			i3_model_posterior(model,mcmc->model_image, current_best, data_set);
		}
		else{ //Check to see if the best MCMC sample is better than the best minimizer sample.  If so, use it.
			int best_mcmc_sample_index = i3_mcmc_best_index(mcmc);
			if (best_mcmc_sample_index!=-1){  //This indicates that there is a 
											  //reasonable likelihood in the 
											  //chain somewhere (hopefully this is usually true!)			
				i3_flt best_mcmc_like = i3_mcmc_like_number(mcmc, best_mcmc_sample_index);
				if (best_mcmc_like > like){
					if (options->verbosity>2) printf("MCMC Better  (%f)\n\n\n",best_mcmc_like-like);					
					
					like = best_mcmc_like;
					i3_model_copy_parameters(model, current_best, i3_mcmc_sample_number(mcmc,best_mcmc_sample_index));
					i3_model_posterior(model,mcmc->model_image, current_best, data_set);
					data_set->signal_to_noise = i3_signal_to_noise_model_fitted(image,mcmc->model_image);
				}
				else{
					if (options->verbosity>2) printf("MCMC Worse  (%f)\n\n\n",best_mcmc_like-like);
					
				}
			}
		}
		i3_mcmc_destroy(mcmc);
	}
	



	//Call the funtion above to print out the result
	char * output_dir=NULL;
	if (options->save_images) output_dir = options->output_directory;
	
	save_result(model, current_best,minimizer->model_image, image, like, output_dir, output_file, identifier, data_set);

	//Free up memory that we allocated.
	i3_minimizer_destroy(minimizer);
	free(current_best);
	free(data_set);

}



int main(int argc, char * argv[]){



	if(argc<4){fprintf(stderr,"Usage: %s parameter_filename  image_file  psf_file\n\n",argv[0]);exit(1);}
	char * parameter_file = argv[1];
	char * input_image_filename = argv[2];
	char * input_psf_filename = argv[3];
	char * output_filename = argv[4];
	
	i3_fftw_load_wisdom();
	atexit(i3_fftw_save_wisdom);
	i3_init_rng();

	i3_options * options = i3_options_default();
	i3_options_read(parameter_file, options);
	
	i3_image * full_image = i3_read_fits_image(input_image_filename);
	if (!full_image) I3_FATAL("Could not load image from fits file.",1);
	int npsf;
	i3_moffat_psf * psf_cat = i3_great10_psf_catalog_read(input_psf_filename, &npsf);

	int n = options->stamp_size;
	// int n1 = n + 1;

		
	i3_model * model = i3_model_create(options->model_name,options);
	// i3_parameter_set * initial_parameters = i3_model_option_starts(model,options);


        fitsfile * input_image_fits;
        int status=0;
        fits_open_file(&input_image_fits, input_image_filename, READONLY, &status);


	//Make a dummy weight image
	i3_image * weight_stamp = i3_image_create(n,n);
	i3_image_fill(weight_stamp,pow(options->noise_sigma,-2));

	//The downsampling kernel is always the same
	// i3_fourier * downsampler = i3_fourier_downsampling_psf(n1, n1, options->upsampling, n1);


	FILE * output = fopen(output_filename,"w");
	fprintf(output,"# ID          like         %s\n",i3_model_header_line(model));
	
	
	int image_size = GREAT10_IMAGE_SIZE;
	//	int image_size = 40;

	for(int i=0;i<GREAT10_ROWS_PER_FILE;i++){
		for(int j=0;j<GREAT10_IMAGES_PER_ROW;j++){
			//Get the postage stamp to analyze
			
//			i3_image * galaxy = i3_image_copy_part(full_image,image_size*i,image_size*j,image_size,image_size);
			i3_image * galaxy = i3_image_copy_part(full_image,image_size*j,image_size*i,image_size,image_size);
			if (!galaxy) I3_FATAL("Failed to get galaxy from full image",2);
			i3_image * stamp = cut_out_stamp(galaxy,options->stamp_size);
                        i3_image * stamp2 = i3_image_create(image_size, image_size);
                        i3_image_zero(stamp2);
                        long lower_left[2] = {image_size*i, image_size*j};
			long upper_right[2] = {image_size*(i + 1), image_size*(j + 1)};
			float null_value=0.0;
			int any_null;
			int stat;
                        int i3_fitsio_type;
			long inc=1;
			i3_fitsio_type = i3_get_fitsio_flt_dtype();
                        fits_read_subset(input_image_fits, i3_fitsio_type, lower_left, upper_right, &inc, &null_value, &stamp2->data, &any_null, &stat);

			i3_image_save_fits(stamp, "stamp.fits");
			i3_image_save_fits(stamp2, "stamp2.fits");
			

			if (!stamp) I3_FATAL("Failed to cut stamp out from galaxy",3);
			//Analyze the image.
			int identifier=j+i*options->great08_cols+1;
			i3_moffat_psf * this_psf = psf_cat + (identifier-1);

                        i3_image * psf_image = i3_image_create(image_size, image_size);
			i3_image_zero(psf_image);
                        printf("%d %d\n", this_psf->x, this_psf->y);
			i3_image_add_truncated_moffat(psf_image, 24., 24., i3_moffat_radius(this_psf->fwhm, this_psf->beta), this_psf->e1, this_psf->e2, this_psf->beta, 100000.);
                        i3_image_save_fits(psf_image, "psf.fits");
	        	if(i==0 && j==50){I3_FATAL("Quitting just to take a look at what galaxy we have in the bottom left!", 1);} else { };
			//			analyze_image(stamp,this_psf,weight_stamp,model,options,output,identifier);
	
			//Clean up
			fflush(output);
			i3_image_destroy(stamp);
			i3_image_destroy(galaxy);
                        i3_image_destroy(stamp2);
                        i3_image_destroy(psf_image);
		}
	}

	return 0;
	
}


