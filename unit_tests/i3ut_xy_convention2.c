#include "i3_image.h"
#include "i3_load_data.h"
#include "i3_image_fits.h"
#include "i3_model_tools.h"


int main(int argc, char * argv[]){
	char * output_filename = argv[1];
	int nx = 50;
	int ny = 100;

	i3_image * image = i3_image_create(nx,ny);
	i3_image_zero(image);


	i3_flt x0 = atof(argv[2]);
	i3_flt y0 = atof(argv[3]);

	i3_flt ab = 20.0;
	i3_flt e1 = 0.0;
	i3_flt e2 = 0.0;
	i3_flt A = 1.0;
	i3_flt index = 1.0;
	i3_flt factor = 1e30;
	int n_central = 5;
	int n_central_upsampling = 5;

	i3_add_real_space_sersic_truncated_radius_upsample_central(ab,e1,e2,A,x0,y0,index,factor,n_central,n_central_upsampling,  image);

	printf("i3ut_xy_convention2: created an image with center %f %f (remember that DS9 iterates from 1)\n",x0,y0);

	int status = i3_image_save_fits(image, output_filename);
	printf("i3ut_xy_convention2: saved image with status %d\n",status);
	return status;
}