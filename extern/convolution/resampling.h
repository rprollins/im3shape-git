#ifndef RESAMPLING_H
#define RESAMPLING_H

#include "defs.h"

resample2mat * resample2_matrix(size sin, size sout);
cs_matrix * resample_matrix (int in, int out);
matrix * resample2(resample2mat * R, matrix * in);
matrix * resample2tp(resample2mat * R, matrix * in);
#endif
