#ifndef CNV2_H
#define CNV2_H

#include "defs.h"

cnvmat * cnv2mat(matrix*, size, char*);
matrix * cnv(cnvmat*, matrix*);
matrix * cnvtp(cnvmat*, matrix*);
matrix * pad(matrix*, size, size);
matrix * crop(matrix*, size, size);
#endif
