#ifndef UTILS_CU
#define UTILS_CU

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "utils.h"
#include "defs.h"

double timeDiff(struct timeval start, struct timeval end){
  return (double)(end.tv_sec+end.tv_usec/1e6 - start.tv_sec - start.tv_usec/1e6); 
}

void readData(char *dataFile, matrix data){ //int rows, int cols, real *data){
  FILE *fp;
  int numRead;

  fp = fopen(dataFile,"r");
  if(fp==NULL){
    fprintf(stderr,"error opening file.. exiting\n");
    exit(1);
  }

  numRead = 0;
  for(int i=0; i< data.size.m; i++){
    numRead += fread(&data.mat[IDX( i, 0, data.size.n )], sizeof(real), data.size.n, fp);
  }

  if(numRead != data.size.m * data.size.n ){
    fprintf(stderr,"error reading file.. exiting \n");
    exit(1);
  }
  fclose(fp);
}


void writeDataText(matrix y, char* dataFileOut){
  FILE *fp;
  fp = fopen(dataFileOut,"w");
  if(fp==NULL){
    fprintf(stderr,"error opening file.. exiting\n");
    exit(1);
  }

  printf("Saving image to disk..");
  for(int i=0; i<y.size.m; i++){
    for(int j=0; j<y.size.n; j++){
      fprintf(fp, "%f ", y.mat[IDX( i, j, y.size.n )]);
    }
    fprintf(fp,"\n");
  }
  
  fclose(fp);
  printf("Done!\n");
}

#endif
