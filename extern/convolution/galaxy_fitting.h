/* ************************** 
   galaxy_fitting.c

   Created on: July 7, 2011
   Author: Michael Hirsch
   ************************** */
#ifndef GALAXY_FITTING_H
#define GALAXY_FITTING_H

#include "defs.h"

void check_gradient(function, int, real*, void*);
void dot2x2(real*, real*, real*);
void q2p(real*, real*);
void q2cov(real*, real*);
// Gaussian fitting routines
void fg_x2parameters(real*, real*, real*, real*, real*);
void gaussian(real, real, real, real*, matrix*);
void fg_function(real*, real*, real*, void*);
void fg(real*, matrix*);
// Fitting routine for Gaussian incl. linear forward model
void fcg_function(real*, real*, real*, void*);
void fcg(real* , matrix*, generative_model*);
// Generalized Gaussian fitting routines
void fgg_x2parameters(real*, real*, real*, real*, real*, real*);
void generalized_gaussian(real, real, real, real*, real, matrix*);
void fgg_function(real*, real*, real*, void*);
void fgg(real*, matrix*);
// Fitting routine for Gaussian incl. linear forward model
void fcgg_function(real*, real*, real*, void*);
void fcgg(real* , matrix*, generative_model*);
#endif
