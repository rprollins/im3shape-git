#ifndef _H_I3_MATH
#define _H_I3_MATH
#include "i3_gsl_random.h"
#include "math.h"
#include "float.h"
#include "i3_global.h"
#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

#define BAD_LIKELIHOOD (-FLT_MAX)

//#include "complex.h"
//#include "fftw3.h"


/**
 * Simple copy array into from source to destination. The arrays must be of the same size.
 * \param src Source array
 * \param dst Destination array
 * \param n size of both arrays
 * 
 */
void i3_array_copy_into(i3_flt * dst, i3_flt * src, int n );


/**
 * Generate a random n-dimensional rotation.
 * 
 * Uses an algorithm from CosmoMC.  
 * \param R The space to hold the rotation.  Should be allocated with space for n^2 i3_flts
 * \param n The dimension of the rotation to generate.
 */

void i3_random_rotation(i3_flt * R, int n);

/**
 * Generate an n-dimensional identity matrix.
 * 
 * \param M The space to hold the identity matrix.  Should be allocated with space for n^2 i3_flts
 * \param n The dimension of the matrix to generate.
 */
void i3_identity_matrix(i3_flt *M, int n);

/**
 * Return the dot product between two vectors.
 * 
 * \param a The first vector
 * \param b The second vector
 * \param n The dimension of the vectors.
 */
i3_flt i3_array_dot(i3_flt * a, i3_flt * b, int n);

/**
 * Return the norm of a vector.
 * 
 * \param a The vector whose norm to return
 * \param n The dimension of the vector.
 */
i3_flt i3_array_norm(i3_flt * a, int n);


/**
 * Return the phase of a complex ellipticity stored in two variables.
 * 
 * \param e1 The real part
 * \param e2 The imaginary part 
 */
i3_flt i3_abs_e(i3_flt e1, i3_flt e2);

/**
 * Return the angle of a complex ellipticity stored in two variables.
 * 
 * \param e1 The real part
 * \param e2 The imaginary part 
 */
i3_flt i3_angle_e(i3_flt e1, i3_flt e2);


/**
 * Return the value of the first maximum element in the array
 * 
 * \param vect Pointer to an array if i3_flts
 * \param n Size of the array
 */
i3_flt i3_array_max_value(i3_flt * array, int n);

/**
 * Return the value of the first minimum element in the array
 * 
 * \param vect Pointer to an array if i3_flts
 * \param n Size of the array
 */
i3_flt i3_array_min_value(i3_flt * array, int n);

/**
 * Return the index of the first maximum element in the array
 * 
 * \param vect Pointer to an array if i3_flts
 * \param n Size of the array
 */
int i3_array_max_index(i3_flt * array, int n);

/**
 * Return the index of the first minimum element in the array
 * 
 * \param vect Pointer to an array if i3_flts
 * \param n Size of the array
 */
int i3_array_min_index(i3_flt * array, int n);


/**
 * Return the value and index of the max element in the array
 * 
 * \param array Pointer to an array if i3_flts
 * \param n Size of the array
 * \param maxval - pointer to i3_flt to contain the max value
 * \param maxind - pointer to int to contain the index of the max value
 */
void i3_array_max(i3_flt * array, int n, i3_flt * maxval, int * maxind);



/**
 * Return the value and index of the min element in the array
 * 
 * \param array Pointer to an array if i3_flts
 * \param n Size of the array
 * \param minval - pointer to i3_flt to contain the min value
 * \param minind - pointer to int to contain the index of the min value
 */
void i3_array_min(i3_flt * array, int n, i3_flt * minval, int * minind);


/**
 * Return the mean of an array.
 * 
 * \param x The array whose mean to return
 * \param n The number of elements in the array.
 */

i3_flt i3_array_mean(i3_flt *x, int n);

void i3_multiply_rv_rs_into(i3_flt * v, int n, i3_flt s, i3_flt * result);
void i3_multiply_rv_rs(i3_flt * v, int n, i3_flt s);

//void i3_multiply_cv_cv_into(i3_cpx * v1, i3_cpx * v2, int n, i3_cpx * result);	

/**
 * The sinc function.
 * 
 * \param x A value.
 * \return i3_sin(x)/x, or 1 if x==0
 */
i3_flt i3_sinc(i3_flt x);

/**
Returns an output array which contains exponent of input array. To avoid precision issues, the input array is scaled, so that the output array value of maximum value in the input array is 1. The input array is not overwritten.
\param array_log Input array to exponentiate
\param n Size of the input array
*/
i3_flt * i3_array_exp_scale(i3_flt * array_log, int n, i3_flt scale);

/**
 * Normalise an array so that sum of its elements is zero. 
 * Returns the normalisation constant. The input array is overwritten.
 * \param array The array of i3_flts to normalize
 * \param n The length of the array
 * \return The normalizing constant
**/
i3_flt i3_array_normalise(i3_flt * array, int n);

/**
 * Compute the sum of all values in an array.
 * \param array The array of i3_flts.
 * \param n The length of the array
 * \return The sum of the values in the array
**/
i3_flt i3_array_sum(i3_flt * array, int n);

int i3_array_add(i3_flt * dst, i3_flt * src, int n);

int i3_array_zero(i3_flt * array, int n);


void i3_matrix2_square_invert(i3_flt * q, i3_flt * p);

void i3_matrix_2_square(i3_flt * q, i3_flt * cov);


void i3_matrix_2_multiply(i3_flt * x, i3_flt * y, i3_flt * z);

void i3_matrix_2_cholesky(i3_flt * p, i3_flt * q);

void i3_matrix_2_invert(i3_flt * p,i3_flt * cov);
void i3_matrix_3_multiply(i3_flt * x, i3_flt * y, i3_flt * z);	
void i3_cholesky_jacobian(i3_flt * p, i3_flt * q, i3_flt * J);


#ifdef USE_LAPACK
int i3_matrix_cholesky_sqrt(i3_flt * A, i3_flt * L, int n);
int i3_covmat_eigenvectors(i3_flt * A, int n);
int i3_covmat_eigensystem(i3_flt * A, i3_flt * e, int n);
#endif

void i3_array_int_sort(int * A, int n);


#endif


