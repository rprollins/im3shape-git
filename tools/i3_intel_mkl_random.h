#ifndef _H_I3_INTEL_MKL_RANDOM
#define _H_I3_INTEL_MKL_RANDOM


void i3_intel_init_rng();
void i3_intel_init_rng_multiprocess(int seed);
float i3_intel_random_uniform_float();
double i3_intel_random_uniform_double();
float i3_intel_random_normal_float();
double i3_intel_random_normal_double();

#endif
