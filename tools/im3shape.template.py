import ctypes
import numpy as np
import os


root_dir=os.path.abspath(os.path.split(__file__)[0])
lib_path=os.path.join(root_dir,'..','lib','libim3.so')
lib=ctypes.cdll.LoadLibrary(lib_path)


if lib.i3UseDouble:
	c_flt=ctypes.c_double
	i3_flt=np.float64
else:
	c_flt=ctype.c_float
	i3_flt=np.float32

c_flt_p=ctypes.POINTER(c_flt)
c_void_pp = ctypes.POINTER(ctypes.c_void_p)
#python struct_code



class MoffatParams(ctypes.Structure):
		_fields_ = [
			("beta",c_flt),
			("fwhm",c_flt),
			("e1",c_flt),
			("e2",c_flt),
			("x",ctypes.c_int),
			("y",ctypes.c_int),
		]


# Extract the functions we want from the library
# Based on these signatures:
# int i3_binding_create_model_options(i3_model ** model, i3_options ** options);
# void i3_binding_set_option(i3_options *options, char *name, char *value);
# int i3_binding_analyze(int nx, int ny, i3_model * model, i3_options * options, 
# 	i3_flt * image_data, i3_flt * psf_data, i3_flt * weight_data, 
# 	i3_flt * fit_image_data, i3_flt * like, i3_parameter_set ** result);

_i3_binding_create_model_options=lib.i3_binding_create_model_options
_i3_binding_create_model_options.restype=ctypes.c_int
_i3_binding_create_model_options.argtypes=[c_void_pp,c_void_pp,ctypes.c_char_p]

_i3_binding_analyze=lib.i3_binding_analyze
_i3_binding_analyze.restype=ctypes.c_int
_i3_binding_analyze.argtypes=[ctypes.c_int, ctypes.c_int, ctypes.c_void_p, ctypes.c_void_p, 
	c_flt_p, c_flt_p, c_flt_p,
	c_flt_p, c_flt_p, c_void_pp,
]

_i3_binding_set_option=lib.i3_binding_set_option
_i3_binding_set_option.restype=None
_i3_binding_set_option.argtypes=[ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]

_i3_options_read=lib.i3_options_read
_i3_options_read.restype=None
_i3_options_read.argtypes=[ctypes.c_char_p, ctypes.c_void_p]

_i3_binding_moffat_psf = lib.i3_binding_moffat_psf
_i3_binding_moffat_psf.restype=ctypes.c_int
_i3_binding_moffat_psf.argtypes=[ctypes.c_int, ctypes.c_int, ctypes.c_int, c_flt, ctypes.POINTER(MoffatParams), c_flt_p]


def moffat_psf(npix, upsampling, padding, beta, fwhm, e1, e2, truncation=10000.0):
	n = (npix+padding)*upsampling
	psf = np.zeros((n,n))
	params = MoffatParams()
	params.beta = beta
	params.fwhm = fwhm
	params.e1 = e1
	params.e2 = e2
	params.x = 0
	params.y = 0
	status = _i3_binding_moffat_psf(npix, upsampling, padding, truncation, ctypes.byref(params), psf.ctypes.data_as(c_flt_p))
	return psf

class I3Analysis(object):
	def __init__(self, param_file, **kwargs):
		self.model = ctypes.c_void_p()
		self.options = ctypes.c_void_p()
		r=_i3_binding_create_model_options(ctypes.byref(self.model), ctypes.byref(self.options),param_file)
		assert r==0, "Error in binding C-Python"
	def __setitem__(self, name, value):
		_i3_binding_set_option(self.options, name, value)

	def comparison_plot(self, image, fit):
		import pylab
		vmin = min((fit-image).min(), fit.min(), image.min())
		vmax = max((fit-image).max(), fit.max(), image.max())
		fig=pylab.figure()
		pylab.subplot(131)
		pylab.imshow(image, interpolation='nearest', vmin=vmin, vmax=vmax)
		pylab.subplot(132)
		pylab.imshow(fit, interpolation='nearest', vmin=vmin, vmax=vmax)
		pylab.subplot(133)
		im=pylab.imshow(fit-image, interpolation='nearest', vmin=vmin, vmax=vmax)
		cax = fig.add_axes([0.1, 0.1, 0.8, 0.03])
		pylab.colorbar(im, cax=cax, orientation='horizontal')
		return fig
	

	def analyze(self, image, psf, weight=None):
		#Convert the image and PSF to the correct type
		norm = image.sum()
		nx,ny=image.shape
		image=image.astype(i3_flt)/norm
		psf=psf.astype(i3_flt)
		#If no weight is present, fake one.
		if weight is None:
			weight=np.ones((nx,ny))

		#Then convert to the correct type
		weight=weight.astype(i3_flt)

		#Prepare pointers to the output space
		like = c_flt()
		fit_image = np.zeros((nx,ny))
		result = ctypes.c_void_p()

		#Run the main wrapper code
		_i3_binding_analyze(nx, ny, self.model, self.options, image.ctypes.data_as(c_flt_p), psf.ctypes.data_as(c_flt_p), weight.ctypes.data_as(c_flt_p), fit_image.ctypes.data_as(c_flt_p), ctypes.byref(like), ctypes.byref(result))

		#Convert results back to python and return		
		result = SersicsParams.from_address(result.value)
		like = like.value
		return fit_image*norm, like, result

def struct_to_dict(struct):
	d ={}
	for name,ptype in struct._fields_:
		d[name] = getattr(struct, name)
	return d


def test1():
	import pylab
	a = I3Analysis("./examples/params.ini")
	x,y = np.mgrid[-32:32, -32:32]
	img = np.exp(-0.5*(x**2+y**2)/5**2 )
	psf = np.exp(-0.5*(x**2+y**2)/1**2 )
	psf/=psf.sum()
	fit_image, like, result =  a.analyze(img, psf)
	print "Like: ", like
	print "Results:"
	print result
	import pylab
	a.comparison_plot(img, fit_image)
	pylab.show()

def test2():
	import pylab
	psf=moffat_psf(64, 5, 2, 3.0, 5.0, 0.3, 0.0, 100.0)
	pylab.imshow(psf)
	pylab.show()


if __name__=="__main__":
	test2()
