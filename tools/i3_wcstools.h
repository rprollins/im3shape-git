#include "stdio.h"
#include "fitsfile.h"
#include "wcs.h"
#include "i3_wcs.h"


struct WorldCoor * GetWCSFITS (char * filename, int verbose);

i3_transform i3_get_wcs_to_image_transform(i3_flt ra, i3_flt dec, struct WorldCoor * w);

i3_transform i3_get_image_to_wcs_transform(i3_flt x, i3_flt y, struct WorldCoor * w);
