#ifndef _H_I3_LOGGING
#define _H_I3_LOGGING

#include "stdlib.h"
#include "stdio.h"


#define i3_verb_silent -1
#define i3_verb_quiet 0
#define i3_verb_standard 1
#define i3_verb_noisy 2
#define i3_verb_debug 3
#define i3_verb_extreme 4
#define i3_verb_crazy 5


/**
 * If the global verbosity is greater than or equal to min_verbosity,
 * print the message and arguments, which take printf-style arguments.
 * Defined values of the verbosity are:
 * i3_verb_silent   = -1
 * i3_verb_quiet    =  0
 * i3_verb_standard =  1
 * i3_verb_noisy    =  2
 * i3_verb_debug    =  3
 * i3_verb_extreme  =  4
 * i3_verb_crazy    =  5
**/
void i3_print(int min_verbosity, const char * message, ...);

/**
 * Set the global verbosity level.
 * Future calls to i3_log compare their min_verbosity to the global verbosity
 * to see whether or not to print the message.
 * Defined values are:
 * i3_verb_silent   = -1
 * i3_verb_quiet    =  0
 * i3_verb_standard =  1
 * i3_verb_noisy    =  2
 * i3_verb_debug    =  3
 * i3_verb_extreme  =  4
 * i3_verb_crazy    =  5
 * 
**/
void i3_set_verbosity(int verbosity);
int i3_get_verbosity();
int i3_should_print(int min_verbosity);

enum {I3_SUCCESS, I3_PARAMETER_ERROR, I3_MATH_ERROR, I3_IO_ERROR};

/**
 * You do not need to use this function directly.
 * Use the macros I3_FATAL defined in i3_logging.h instead.
**/
void i3_fatal_error(const char * filename, int line_number, const char * message, int return_code);


/**
 * You do not need to use this function directly.
 * Use the macro I3_WARNING defined in i3_logging.h instead.
**/
void i3_warning(const char * filename, int line_number, const char *message);




/**
* \def I3_FATAL(message,return_code)
* Trigger a fatal error that should kill the program.  
* \param message The message printed to stderr before exiting.  The filename and line number are also recorded.
* \param return_code The error code to return to the shell
*/
#define I3_FATAL(message,return_code) i3_fatal_error(__FILE__,__LINE__,message,return_code)


/**
* \def I3_WARNING(message)
* Trigger a warning to the user.  The code continues to run.  
* \param message The message printed to stderr.  The filename and line number are also recorded.
*/
#define I3_WARNING(message) i3_warning(__FILE__,__LINE__,message)


#endif