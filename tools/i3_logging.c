#include "i3_logging.h"
#include "stdarg.h"

static int i3_logging_verbosity=i3_verb_standard;

void i3_set_verbosity(int verbosity){
	i3_logging_verbosity=verbosity;
}

int i3_get_verbosity(){
	return i3_logging_verbosity;
}

void i3_fatal_error(const char * filename, int line_number, const char * message, int return_code){
	fprintf(stderr,"im3shape ended following a fatal error.\nThis occurred at line %d in file %s.\nThe message was:\n%s\n\n",line_number,filename,message);
	exit(return_code);
}

void i3_warning(const char * filename, int line_number, const char *message){
	fprintf(stderr,"Warning (line %d of %s).\n%s\n",line_number,filename,message);
}

void i3_print(int min_verbosity, const char * message, ...){
	if (i3_logging_verbosity>=min_verbosity){
		va_list args;
		va_start(args,message);
		vprintf(message,args);
		printf("\n");
	}
	
}

int i3_should_print(int min_verbosity){
	return (i3_logging_verbosity>=min_verbosity);
}



// FILE * i3_log_file = NULL;
// I3_VERBOSITY i3_verbosity;
// 
// void i3_set_log_file(FILE * log_file){
// 	i3_log_file = log_file;
// }
// 
// void i3_set_verbosity(I3_VERBOSITY verbosity){
// 	i3_verbosity = verbosity;
// }

