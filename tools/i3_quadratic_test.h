#ifndef _H_I3_QUADRATIC_TEST
#define _H_I3_QUADRATIC_TEST
#include "i3_quadratic_test_definition.h"
#include "i3_data_set.h"


#define QUADRATIC_TEST_TRUE_MEAN1 3.0
#define QUADRATIC_TEST_TRUE_VAR1 (0.2*0.2)

#define QUADRATIC_TEST_TRUE_MEAN2 10.0
#define QUADRATIC_TEST_TRUE_VAR2 (0.1*0.1)

#define QUADRATIC_TEST_TRUE_MEAN3 0.0
#define QUADRATIC_TEST_TRUE_VAR3 (0.4*0.4)

#define QUADRATIC_TEST_TRUE_MEAN4 -5.0
#define QUADRATIC_TEST_TRUE_VAR4 (1.0*1.0)

i3_flt i3_quadratic_test_likelihood(i3_image * image, i3_quadratic_test_parameter_set * parameters, i3_data_set * dataset);
void i3_quadratic_test_propose(i3_mcmc_ptr mcmc,i3_quadratic_test_parameter_set * current,i3_quadratic_test_parameter_set * proposed);
void i3_quadratic_test_tune_proposal(i3_mcmc_ptr mcmc);
int i3_quadratic_test_parameter_string(i3_quadratic_test_parameter_set * parameter,char * parameter_string,int string_length);
int i3_quadratic_test_parameter_pretty_string(i3_quadratic_test_parameter_set * parameter,char * parameter_string,int string_length);
#endif

