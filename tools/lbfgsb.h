#ifndef LBFGSB_H
#define LBFGSB_H


// Type definitions
// -----------------------------------------------------------------
// This defines the possible results of running the L-BFGS-B solver.
enum SolverExitStatus { 
  success,              // The algorithm has converged to a stationary
                        // point or has reached the maximum number of
                        // iterations.
  abnormalTermination,  // The algorithm has terminated abnormally and
                        // was unable to satisfy the convergence
                        // criteria.
  errorOnInput          // The routine has detected an error in the
                        // input parameters.
};



// Function declarations.
// -----------------------------------------------------------------
// This is the L-BFGS-B routine implemented in Fortran 77.
void setulb_ (int* n, int* m, double x[], double l[], 
			 double u[], int nbd[], double* f, double g[], 
			 double* factr, double* pgtol, double wa[], 
			 int iwa[], char task[], int* iprint, 
			 char csave[], int lsave[], int isave[], 
			 double dsave[]);


typedef void (*lbf_function)(double *x, double * f, double * gradient, void * data);
int lbf_solve(int n, double * x0, double * lb, double * ub, lbf_function function, void * auxdata, int verbosity, int max_iterations, double accuracy);





#endif


