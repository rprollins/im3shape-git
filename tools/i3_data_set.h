#ifndef _H_I3_DATA_SET
#define _H_I3_DATA_SET
#include "i3_image.h"
#include "i3_psf.h"
#include "i3_options.h"
#include "i3_wcs.h"
#include <stdbool.h>


#define MAX_N_EXPOSURE 100


typedef struct i3_exposure{
	int index;
	i3_transform transform;
	i3_fourier * kernel;
	i3_image * image;
	i3_image * weights;
	i3_flt x;
	i3_flt y;
	i3_flt e1;
	i3_flt e2;
	i3_flt chi2;
	i3_flt residual_stdev;
	char band;
} i3_exposure;

/**
 * A structure representing the data-set to be passed to the likelihood.
 * The very simplest this can be is just the data image and the noise standard deviation,
 * but for more realistic models it contains the point spread function or its fourier transoform,
 * a complete kernel for the PSF and downsampling, multiple exposure images
 * and possibly a noise matrix.
 * 
 * The version currently implemented has some of these things; not all are needed for any one likelihood function
**/
typedef struct i3_data_set{
	gal_id identifier;/**< An identifying number for the galaxy */
	i3_image * image;   /**< A single data image that we want to fit*/
	i3_image * PSF; /**< The point-spread function as an image */
	i3_image * weight; /**< Weight map for the image: inverse variance of each pixel*/
	i3_fourier * psf_downsampler_kernel; /**< The combined PSF and sinc function downsampler (FFT'd top hat) fourier space object*/
	i3_moffat_psf * psf_form; /**< Moffat PSF parameter set*/
	
	int stamp_size;	/**< Size of the postage stamp*/
	int upsampling; /**< The factor higher at which models are generated than the data*/
	int padding; /** padding used: the image is created with n_all=n_pix+n_pad, the likelihood only uses n_pix. Purpose: avoid aliasing and multiple imaging when doing circular convolution*/	
	bool central_pixel_upsampling; /**< Whether to do extra upsampling in central pixels of galaxy image - N.B. Only implemented in milestone model*/
	int n_central_pixel_upsampling; /**< Factor by which to upsample the high-resolution pixels, in the center of the galaxy image*/
	int n_central_pixels_to_upsample; /**< If 1 it will upsample only central pixel, if 2 also its one neighbor on both sides, etc.*/
	
	int n_logL_evals; /** can be used to increment each time the likelihood function is called to count the number of calls to the likelihood function*/
	int n_logL_excluded; /** can be used to count how many times the likelihood function is excluding parameter as out of range*/
	int n_logL_warning; /** can be used to count the number of warnings inside likelihood function (defined according to the user) */
	int n_params_result_same_as_start; /** can be used to count the number of warnings inside likelihood function (defined according to the user) */
	
	i3_flt * levmar_info; /** Levmar info, modified to include information about additional termination criteria 
		      *		O: information regarding the minimization. Set to NULL if don't care
                      * info[0]= ||e||_2 at initial p.
                      * info[1-5]=[ ||e||_2, ||J^T e||_inf,  ||Dp||_2, mu/max[J^T J]_ii ], D||e||_2, all computed at estimated p.
                      * info[6]= # iterations,
                      * info[7]=reason for terminating: 1 - stopped by small gradient J^T e
                      *                                 2 - stopped by small Dp
                      *                                 3 - stopped by itmax
                      *                                 4 - singular matrix. Restart from current p with increased mu 
                      *                                 5 - no further error reduction is possible. Restart with increased mu
                      *                                 6 - stopped by small ||e||_2
                      *                                 7 - stopped by invalid (i.e. NaN or Inf) "func" values. This is a user error
		      *                                 8 - stopped by small D||e||_2 
                      * info[8]= # function evaluations
                      * info[9]= # Jacobian evaluations
                      * info[10]= # linear systems solved, i.e. # attempts for reducing error
                      */

	i3_flt amplitude1, amplitude2; /** obsolete: fitted amplitudes of bulge and disc components*/  
	i3_flt signal_to_noise; /** Signal to noise defined as ||image||_2/sigma_noise */
	i3_flt old_signal_to_noise; /** Signal to noise defined as ||image||_2/sigma_noise */
	i3_flt noise; /** Standard deviation of nosie*/
	i3_flt flux_ratio; /** Measured ratio of fluxes for bulge and disc component models*/
	i3_parameter_set * start_params; /** Starting parameters for LEVMAR, use NULL if you want im3shape to calculate them.*/
	
	i3_options * options; /** im3shape options */

	i3_flt psf_fwhm;

	int n_exposure;
	i3_exposure exposures[MAX_N_EXPOSURE];

	i3_flt * covariance_estimate;
	
} i3_data_set;

/** Sets the counters to zero: n_logL_evals, n_logL_excluded, n_logL_warning, int n_params_result_same_as_start */ 
void i3_dataset_reset_counters(i3_data_set * dataset);
/** Destructor */
void i3_dataset_destroy(i3_data_set * dataset);

int i3_dataset_count_distinct_bands(i3_data_set * dataset);
void i3_dataset_get_distinct_bands(i3_data_set * dataset, int nband, char * bands);
#endif

