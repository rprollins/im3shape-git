#ifndef _H_I3_GSL_RANDOM
#define _H_I3_GSL_RANDOM
#include "i3_global.h"


/**
 * RNG function.  Use the typedef'd one instead (i3_random_uniform) - makes it easier to change RNG.
**/
float i3_gsl_random_uniform_float();

/**
 * RNG function.  Use the typedef'd one instead (i3_random_uniform) - makes it easier to change RNG.
**/
double i3_gsl_random_uniform_double();

/**
 * RNG function.  Use the typedef'd one instead (i3_random_normal) - makes it easier to change RNG.
**/
float i3_gsl_random_normal_float();
/**
 * RNG function.  Use the typedef'd one instead (i3_random_normal) - makes it easier to change RNG.
**/
double i3_gsl_random_normal_double();


/**
 * RNG function.  Use the typedef'd one instead (i3_init_rng) - makes it easier to change RNG.
**/
void i3_gsl_init_rng();
/**
 * RNG function.  Use the typedef'd one instead (i3_init_rng_multiprocess) - makes it easier to change RNG.
**/
void i3_gsl_init_rng_multiprocess(int i);
/**
 * RNG function.  Use the typedef'd one instead (i3_init_rng_multiprocess_environment) - makes it easier to change RNG.
**/
void i3_gsl_init_rng_multiprocess_environment(int i);
/**
 * RNG function.  Use the typedef'd one instead (i3_init_rng_environment) - makes it easier to change RNG.
**/
void i3_gsl_init_rng_environment();
/**
 * RNG function.  
**/
void i3_gsl_init_rng_fixedseed(int i);
/**
 * Initialize the random number generator using the current time.
 * This is not suitable for multi-process runs since the start time might be the same.
 * In that case use i3_init_rng_multiprocess(rank) instead.
**/
#define i3_init_rng i3_gsl_init_rng

/**
 * Initialize the random number generator using the current time plus an integer offset.
 * The offset would typically be the rank of the process, to ensure each has a different seed.
**/
#define i3_init_rng_multiprocess i3_gsl_init_rng_multiprocess

/**
 * Initialize the random number generator using an environment variable as the seed.
 * This is not suitable for multi-process runs since the start time might be the same.
 * In that case use i3_init_rng_multiprocess_environment(rank) instead.
**/
#define i3_init_rng_environment i3_gsl_init_rng_environment

/**
 * Initialize the random number generator using an environment variable as the seed, plus an integer offset.
 * The offset would typically be the rank of the process, to ensure each has a different seed.
**/
#define i3_init_rng_multiprocess_environment i3_gsl_init_rng_multiprocess_environment

/**
 * Initialize the random number generator with a fixed seed.
**/
#define i3_init_rng_fixedseed i3_gsl_init_rng_fixedseed

#ifdef I3_USE_DOUBLE
/**
 * Draw a random number from the normal distribution (gaussian with mean 0, variance 1).
 * Different versions of this function are used depending on whether we are in double-precision mode.
*/
#define i3_random_normal i3_gsl_random_normal_double

/**
 * Draw a random number from a uniform distribution between 0 and 1
 * Different versions of this function are used depending on whether we are in double-precision mode.
*/
#define i3_random_uniform i3_gsl_random_uniform_double
#else
#define i3_random_normal i3_gsl_random_normal_float
#define i3_random_uniform i3_gsl_random_uniform_float
#endif

#endif


