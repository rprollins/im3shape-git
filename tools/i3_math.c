#include "i3_math.h"
//#include "math.h"

void i3_array_copy_into(i3_flt * dst, i3_flt * src, int n ){
	
 	for(int i=0;i<n;i++) dst[i]=src[i];
	
}


/**
* Compute the dot product of two vectors of i3_flt.
* dot = sum_i x_i * y_i
*/
i3_flt i3_array_dot(i3_flt * a, i3_flt * b, int n){
	i3_flt dot = 0.0;
	for(int i=0;i<n;i++) dot += a[i]*b[i];
	return dot;
}


/**
* Compute the norm of a vector of i3_flt.
* norm = sum_i x_i**2
*/
i3_flt i3_array_norm(i3_flt * a, int n){	
	i3_flt norm = 0.0;
	for(int i=0;i<n;i++) norm += a[i]*a[i];
	return i3_sqrt(norm);
	
}


/**
* Compute the mean of a vector of doubles.
*/
i3_flt i3_array_mean(i3_flt *x, int n){
	i3_flt mu = 0.0;
	for(int i=0;i<n;i++) mu += x[i];
	return mu/n;
	
	
}

/**
* Compute the abs of complex number stored in two i3_flt
*/
i3_flt i3_abs_e(i3_flt e1, i3_flt e2){

return i3_sqrt(e1*e1 + e2*e2);

}

/**
* Compute the abs of complex number stored in two i3_flt
*/
i3_flt i3_angle_e(i3_flt e1, i3_flt e2){

return 0.5*i3_atan2(e2,e1);

}

i3_flt i3_array_max_value(i3_flt * array, int n)
{
	i3_flt max = array[0];
	for(int i=1;i<n;i++){
		if(array[i]>max){
			max = array[i];
		}
	}
	return max;
}


i3_flt i3_array_min_value(i3_flt * array, int n)
{
	i3_flt min = array[0];
	for(int i=1;i<n;i++){
		if(array[i]<min){
			min = array[i];
		}
	}
	return min;
}


int i3_array_max_index(i3_flt * array, int n)
{
	int index = 0;
	for(int i=1;i<n;i++){
		if(array[i]>array[index]){
			//printf(" i3_array_max_index: val %4.4f ",(double)vect[index]); printf(" ind %4.4f \n",(double)index);
			index = i;
		}
	}
	return index;
}


int i3_array_min_index(i3_flt * array, int n)
{
	int index = 0;
	for(int i=1;i<n;i++){
		if(array[i]<array[index]){
			index = i;
		}
	}
	return index;
}

void i3_array_min(i3_flt * array, int n, i3_flt * minval, int * minind){

	int index = 0;
	i3_flt value = array[index];

	for(int i=1;i<n;i++){
		if(array[i]<array[index]){
			index = i;
			value = array[i];
		
		}
	}
	
	*minval = value;
	*minind = index;
}

void i3_array_max(i3_flt * array, int n, i3_flt * maxval, int * maxind){

	int index = 0;
	i3_flt value = array[index];

	for(int i=1;i<n;i++){
		if(array[i]>array[index]){
			index = i;
			value = array[i];
		
		}
	}
	
	*maxval = value;
	*maxind = index;

}

/**
* Generate a random rotation matrix in the buffer R.
* This algorithm is stolen from CosmoMC.
*/

void i3_random_rotation(i3_flt * R, int n){
	i3_flt v[n];
	int i,j,k;
	i3_flt L;
	for (j=0;j<n;j++){
		for(;;){
			for(i=0;i<n;i++) v[i]=i3_random_normal();
			for(i=0;i<j;i++) {
				i3_flt dot = i3_array_dot(v,R+n*i,n);
				for(k=0;k<n;k++) v[k]-=dot*R[k+n*i];
			}
			L = i3_array_norm(v,n);
			if (L>1e-3) break;
		}
		for(k=0;k<n;k++) v[k]/=L;
		for(k=0;k<n;k++) R[k+n*j]=v[k];
	}
}

/**
* Generate the identity matrix in the buffer R.
*/

void i3_identity_matrix(i3_flt * I, int n){
	for (int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			if (i==j){
				I[i+n*j]=1.0;
			}
			else{
				I[i+n*j]=0.0;
			}
		}
	}
}

void i3_multiply_rv_rs_into(i3_flt * v, int n, i3_flt s, i3_flt * result){
	
	for(int i=0;i<n;i++) result[i] = v[i]*s;
}

void i3_multiply_rv_rs(i3_flt * v, int n, i3_flt s){
	
	for(int i=0;i<n;i++) v[i] *= s;
}

i3_flt i3_sinc(i3_flt x){
	if (x==0) return 1;
	return i3_sin(x)/x;
}


i3_flt * i3_array_exp_scale(i3_flt * array_log, int n, i3_flt scale){

	i3_flt * array_exp  = malloc(sizeof(i3_flt)*n);
	//scale = -1e6;
	
// 	for(int i = 0; i<n; i++){
// 		if(max < array_log[i] ) max = array_log[i];
// 	}
	
	for(int i = 0; i<n; i++){
		if(array_log[i] - scale < -90) array_exp[i] = 0.0;
		else {
			array_exp[i] = i3_exp(array_log[i] - scale);
		}
	}
	
	return array_exp;

}

i3_flt i3_array_normalise(i3_flt * array, int n){

	i3_flt normalise = 0.;
	for(int i = 0; i<n; i++){
		normalise += array[i];
	}
	for(int i = 0; i<n; i++){
		array[i] /= normalise;
	}
	return normalise;
}

i3_flt i3_array_sum(i3_flt * array, int n){

	i3_flt sum = 0.;
	for(int i = 0; i<n; i++){
		sum += array[i];
	}
	return sum;
}

int i3_array_add(i3_flt * dst, i3_flt * src, int n){

	for(int i = 0; i<n; i++) dst[i] = src[i] + dst[i];
	return 1;
}

int i3_array_zero(i3_flt * array, int n){
	for(int i=0;i<n;i++) array[i] = 0.0;
	return 1;
} 

// void i3_multiply_cv_cv_into(i3_cpx * v1, i3_cpx * v2, int n, i3_cpx * result){	
// 		for(int i = 0; i<n; i++){
// 			result[i] = v1[i] * v2[i];
// 		}
// }
// 


void i3_matrix2_square_invert(i3_flt * q, i3_flt * p){
  /* Transforming the square root of a covariance matrix to a precision matrix */

  /* Computing the covariance matrix first cov = q * q */
  i3_flt covxx = q[0] * q[0] + q[2] * q[2];
  i3_flt covxy = q[0] * q[1] + q[2] * q[3]; 
  i3_flt covyy = q[1] * q[1] + q[3] * q[3]; 

  /* Computing the determinant of covariance matrix for the matrix inverse */
  i3_flt det = covxx * covyy - covxy * covxy;

  /* Computing the matrix inverse of a 2x2 matrix */
  p[0] = covyy / det;
  p[1] = p[2]= -covxy / det;
  p[3] = covxx / det;
}

void i3_matrix_2_square(i3_flt * q, i3_flt * cov){
  /* Transforming the square root of a covariance matrix to a covariance matrix */

  /* Computing the covariance matrix first cov = q * q */
  cov[0] = q[0] * q[0] + q[2] * q[2];
  cov[1] = cov[2] = q[0] * q[1] + q[2] * q[3]; 
  cov[3] = q[1] * q[1] + q[3] * q[3]; 
}


void i3_matrix_2_multiply(i3_flt * x, i3_flt * y, i3_flt * z){
  /* Computing matrix-matrix multilication for 2x2 matrices 
     represented as vectors containing 4 elements */
  z[0] = x[0] * y[0] + x[1] * y[2];
  z[1] = x[0] * y[1] + x[1] * y[3];
  z[2] = x[2] * y[0] + x[3] * y[2];
  z[3] = x[2] * y[1] + x[3] * y[3]; 
}


void i3_matrix_3_multiply(i3_flt * x, i3_flt * y, i3_flt * z){
	for (int i=0;i<3;i++){
		for (int j=0;j<3;j++){
			z[i+3*j] = 0.0;
			for(int k=0;k<3;k++)
				z[i+3*j] += x[i+3*k]*y[k+3*j];
		}
	}
}



void i3_matrix_2_cholesky(i3_flt * p, i3_flt * q){
	q[0] = i3_sqrt(p[0]);
	q[1] = p[1] / q[0];
	q[2] = 0.0;
	q[3] = i3_sqrt(p[3]-q[1]*q[1]);
}
void i3_matrix_2_invert(i3_flt * p,i3_flt * c){
  /* Computing the determinant of covariance matrix for the matrix inverse */
  i3_flt det = p[0] * p[3] - p[1]*p[2];

  /* Computing the matrix inverse of a 2x2 matrix */
	c[0] = p[3] / det;
	c[1] = -p[1] / det;
	c[2] = -p[2] / det;
	c[3] = p[0] / det;
}

// Get the jacobian dq_ij / dp_ij of the 2x2 cholesky decomposition
void i3_cholesky_jacobian(i3_flt * p, i3_flt * q, i3_flt * J)
{
	J[0] = 0.5/i3_sqrt(p[0]);
	J[1] = 0;
	J[2] = 0;

	J[3] = -0.5*q[1]/p[0];
	J[4] = 1.0/q[0];
	J[5] = 0;

	J[6] = 0.5*q[1]*q[1]/q[3];
	J[7] = -q[1]/(q[3]*i3_sqrt(p[0]));
	J[8] = 0.5/q[3];
}



#ifdef USE_LAPACK

#ifdef I3_USE_DOUBLE
#define syev dsyev
#define potrf dpotrf
#else
#define syev ssyev
#define potrf spotrf
#endif

void potrf(const char * uplo, int * N, i3_flt * A, int * LDA, int * info);


int i3_matrix_cholesky_sqrt(i3_flt * A, i3_flt * L, int n)
{
	int N=n;
	for (int i=0; i<n*n; i++) L[i] = A[i];
	int status=0;
	const char * lower_triangular = "L";
	dpotrf(lower_triangular, &N, L, &N, &status);
	// Now fill in bottom with zeros.  Or the top.
	for (int i=0; i<n; i++){
		for (int j=0; j<n; j++){
			if (j>=i) continue; // or possibly the other way around
			L[i*n+j]=0.0;
		}
	}
	return status;
}

// JAZ I have realized we do not need the ones below, but now I have written them...
void syev(const char * jobz, const char * uplo, int * n, i3_flt * A, int * lda,
 i3_flt * eigvals, i3_flt * work, int * iwork, int * info);

// Assuming that A is a covariance matrix (SPD)
// replace A with its eigenvectors with lengths 
// given by the covmat axes.
int i3_covmat_eigenvectors(i3_flt * A, int n)
{
	const char * get_eigv = "V"; // compute eignvalues AND eigenvectors
	const char * upper = "U"; // compute eignvalues AND eigenvectors
	int N = n;
	int worksize = 2*(n+2)*n;
	i3_flt * eigenvalues = malloc(sizeof(i3_flt)*n);
	i3_flt * workspace = malloc(sizeof(i3_flt)*worksize);
	int status = 0;
	syev(get_eigv, upper, &N, A, &N, eigenvalues, workspace, &worksize, &status);
	free(eigenvalues);
	free(workspace);
	return status;
}

int i3_covmat_eigensystem(i3_flt * A, i3_flt * e, int n)
{
	const char * get_eigv = "V"; // compute eignvalues AND eigenvectors
	const char * upper = "U"; // compute eignvalues AND eigenvectors
	int N = n;
	int worksize = 2*(n+2)*n;
	i3_flt * workspace = malloc(sizeof(i3_flt)*worksize);
	int status = 0;
	syev(get_eigv, upper, &N, A, &N, e, workspace, &worksize, &status);
	free(workspace);
	return status;
}


#endif


static int i3_array_int_sort_cmpfunc (const void * a, const void * b)
{
   return ( *(int*)a - *(int*)b );
}


// in-place sort
void i3_array_int_sort(int * A, int n){
   qsort(A, n, sizeof(int), i3_array_int_sort_cmpfunc);
}
