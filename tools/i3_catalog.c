#include "i3_catalog.h"
#include "i3_math.h"
#include "i3_image.h"
#include "i3_psf.h"
#include "stdbool.h"
#include "i3_logging.h"

#ifdef I3_USE_DOUBLE
#define CAT_ROW_FORMAT "%ld %lf %lf"
#define CAT_ROW_PSF_IMAGE_FORMAT "%lf %lf %s" 
#define CAT_ROW_PSF_MOFFAT_FORMAT "%lf %lf %lf %lf"
#else
#define CAT_ROW_FORMAT "%ld %f %f"
#define CAT_ROW_PSF_IMAGE_FORMAT "%f %f %s"
#define CAT_ROW_PSF_MOFFAT_FORMAT "%f %f %f %f"
#endif

bool i3_catalog_line_is_used(char * line){
	gal_id id;
	i3_flt x,y;
	
	//First check that the first non-whitespace character is not a # comment symbol
	for(char *c=line; *c; c++){
		if (*c==' ' || *c=='\t') continue;
		if (*c=='#') return false;
		break;
	}
	//Now check that the three fields we want are present.
	int count = sscanf(line,CAT_ROW_FORMAT, &id,&x,&y);
	return (count==3);
}

int i3_count_catalog_lines(char * filename){
	FILE * catfile = fopen(filename,"r");
	if (!catfile) return 0;
	char line[MAX_CAT_LINE_LENGTH];
	int n=0;
	while (n < MAX_CAT_LENGTH){
		if (NULL==fgets(line,MAX_CAT_LINE_LENGTH,catfile)) break;
		if (i3_catalog_line_is_used(line)) n++;
	}
	fclose(catfile);
	return n;
}

static bool i3_psf_line_is_used(char * line)
{
	i3_flt w,x,y,z;
	
	//First check that the first non-whitespace character is not a # comment symbol
	for(char *c=line; *c; c++){
		if (*c==' ' || *c=='\t') continue;
		if (*c=='#') return false;
		break;
	}
	//Now check that the three fields we want are present.
	int count = sscanf(line,CAT_ROW_PSF_MOFFAT_FORMAT, &w,&x,&y,&z);
	return (count==4);

}


static int i3_count_psf_lines(char * filename){
	FILE * catfile = fopen(filename,"r");
	if (!catfile) return 0;
	char line[MAX_CAT_LINE_LENGTH];
	int n=0;
	while (n < MAX_CAT_LENGTH){
		if (NULL==fgets(line,MAX_CAT_LINE_LENGTH,catfile)) break;
		if (i3_psf_line_is_used(line)) n++;
	}
	fclose(catfile);
	return n;
}



void i3_catalog_destroy(i3_catalog * cat){
	if (cat->row) free(cat->row);
	free(cat);
}

i3_catalog * i3_catalog_read(char * filename)
{
	//Count the lines and allocate basic space.
	int n = i3_count_catalog_lines(filename);
	i3_catalog * output = malloc(sizeof(i3_catalog));
	output->n = n;
	

	//If the catalog is empty we still return an
	//empty catalog.  This should make it easier to 
	//deal with multiple catalogs that might be empty.
	//We NULL the field just in case.
	if (n==0){
		output->row = NULL;
		return output;
	}
	else{
		output->row = malloc(sizeof(i3_catalog_row)*n);
	}
	
	//Now actually read the catalog
	char line[MAX_CAT_LINE_LENGTH];
	FILE * catfile = fopen(filename,"r");
	for (int i=0; i<n; i++){
		//Read the line and check it read okay and we are not at the end.
		//Though this should not happen if we counted right
		if (NULL==fgets(line,MAX_CAT_LINE_LENGTH,catfile)) break; 
	 	// Skip comment or empty line, and do not count that line.
		if (!i3_catalog_line_is_used(line)) {i--; continue;}
		//Fill in the catalog row.
		i3_catalog_row * row = output->row + i;
		row->catalog_row = i;
		sscanf(line,CAT_ROW_FORMAT, &(row->id), &(row->x), &(row->y)); //The fields in the output are whole arrays
	}
	//If in noisy mode report back.
	i3_print(i3_verb_noisy,"Read %d rows from catalog %s",output->n,filename);
	return output;
}


i3_moffat_psf * i3_psf_catalog_read(char * filename, int *n)
{
  /* 
   * read in catalogue file 
   */

  FILE *fl;
  char line[MAX_CAT_LINE_LENGTH];
  int counter;

  *n = i3_count_psf_lines(filename);
  i3_moffat_psf *output = malloc(sizeof(i3_moffat_psf)*(*n));

  if (*n==0) return NULL;

  fl = fopen(filename,"r");	     
  if (fl == NULL) {
     printf("\nim3shape : Failed to open file: %s", filename);
     exit(-1);
  }
   /* get each line */
  counter =0;
  /* loop forever */
  for(;;){
    /* if first element of a line = # */
     fgets(line, MAX_CAT_LINE_LENGTH, fl);
     if (line[0] == '#'){
       counter += 1;
       // printf("%i\n",counter);
     } else {
      /* else break */
     break;
     }
  }
  int i=0;
	(output+i)->x = 0.0;
	(output+i)->y = 0.0;
  sscanf(line,CAT_ROW_PSF_MOFFAT_FORMAT,&((output+i)->beta),&((output+i)->fwhm),&((output+i)->e1),&((output+i)->e2));
  for (i=1;i<*n;i++){
	(output+i)->x = 0.0;
	(output+i)->y = 0.0;
    fgets(line, MAX_CAT_LINE_LENGTH, fl);
  	sscanf(line,CAT_ROW_PSF_MOFFAT_FORMAT,&((output+i)->beta),&((output+i)->fwhm),&((output+i)->e1),&((output+i)->e2));
  }
  return output;
};
