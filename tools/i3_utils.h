#ifndef _H_I3_UTILS
#define _H_I3_UTILS

#include "i3_global.h"
#include <stdbool.h>
#include "stdlib.h"

/**
 * Check if the named file exists
 * /param filename The file to check
 * /return true if the file exists and is accessible, false otherwise
**/
bool i3_file_exists(char * filename);
const char * i3_get_version();

#ifdef LAPACK_PATH_CHECK
int i3_lapack_path_check();
#endif

#endif
