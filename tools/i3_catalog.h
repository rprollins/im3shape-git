#ifndef _H_I3_CATALOG
#define _H_I3_CATALOG
#include "i3_global.h"
#include "i3_psf.h"

/**
i3_catalog.h
Created by SLB.  01/12.

Defines the i3_catalog and i3_catalog_row structs which contains a catalog or line thereof.

Defines a catalog reader i3_catalog_read which reads from a file into the catalog structs.
**/

/**
 * The row of a simple catalog.
 * catalog_row is the line number of the catalog file that the object was found on 
 *    (ignoring commented lines)
 * id is the object id
 * x is the x position of the object in pixels
 * y is the y position of the object in pixels
**/

#define MAX_CAT_LINE_LENGTH 1024
#define MAX_CAT_LENGTH 500000

typedef struct i3_catalog_row {
	i3_flt  x; /** Estimated centroid x-coordinate array */
	i3_flt  y; /** Estimated centroid y-coordinate array*/
	gal_id  id; /** ID number of galaxy*/
	gal_id catalog_row; /** Row of catalogue file*/
} i3_catalog_row;


/**
 * A collection of rows from a simple catalog.
 * row is a particular row
 * n is the number of rows in the catalog
**/
typedef struct i3_catalog{
	i3_catalog_row * row; /** The rows in the catalog*/
	int n; /** The number of rows in the catalog*/
} i3_catalog;


/**
 * Free the memory in a catalog
 * \param cat The catalog
**/
void i3_catalog_destroy(i3_catalog * cat);


/**
 * Read in a simple catalog file into a pointer to the i3_catalog struct
 * \param filename The name of the file to read from
**/
i3_catalog * i3_catalog_read(char * filename);


/**
 * Count the number of (non-hashed) lines in a catalogue file
 * \param filename The name of the file to read from
**/
int i3_count_catalog_lines(char * filename);

/**
 * Read in a simple psf catalog file into a pointer to the i3_moffat_psf struct
 * \param filename The name of the file to read from
**/
i3_moffat_psf * i3_psf_catalog_read(char * filename, int *n);

#endif

